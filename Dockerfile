# Stage 1
FROM node:carbon as react-build
WORKDIR /app
COPY . ./
RUN yarn
RUN yarn build

# Stage 2 - the production environment
FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=react-build /app/DIST /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
