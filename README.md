## Prerequisites

- NodeJs
- npm or yarn

## Installation
### 0. Create local env file

```
    cp .env.example .env.local
```

### 1. Run project

first time:

```
    make install
```

the next times:

```
    make run
```

## Notes:

### build the project:

```
	make build
```

### clean node_modules and dist

```
	make clean
```

### Project linting and tests:

```
	make test
	make lint
```
