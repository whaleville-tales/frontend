import React from 'react';
import ReactDOM from 'react-dom';
import './assets/scss/style.scss';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';
import { setContext } from 'apollo-link-context';
import Cookies from 'js-cookie';
import { BrowserRouter } from 'react-router-dom';
import { ApolloLink, from } from 'apollo-link';
import config from './helpers/config';
import * as serviceWorker from './serviceWorker';


const App = require('./app').default;

const httpLink = createHttpLink({ uri: config.API_URL });

const authLink = setContext((_, { headers }) => {
  const token = Cookies.get('token');
  if (token) {
    return {
      headers: {
        ...headers,
        authorization: `JWT ${token}`,
      },
    };
  }

  return {
    headers: {
      ...headers,
    },
  };
});

const authMiddleware = new ApolloLink((operation, forward) => {
  const token = Cookies.get('token');
  if (!token && (!window.location.toString().includes('authentication'))) {
    window.location.href = '/authentication/login';
  }

  return forward(operation);
});


const client = new ApolloClient({
  link: from([
    authMiddleware,
    authLink,
    httpLink]),
  cache: new InMemoryCache(),
});


ReactDOM.render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </BrowserRouter>,
  document.getElementById('root'),
);
serviceWorker.unregister();
