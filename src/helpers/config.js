const config = {};

switch (window.location.hostname) {
  case 'whaleville.space':
    config.API_HOST = 'https://api.whaleville.space';
    config.API_WS = 'wss://api.whaleville.space/xhr/';
    break;
  default:
    config.API_HOST = 'http://localhost:8000';
    config.API_WS = 'ws://localhost:8000/xhr/';
    break;
}

config.API_URL = `${config.API_HOST}/graphql`;

config.POLL_INTERVAL = 3500;

export default config;
