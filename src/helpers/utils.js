/* eslint-disable global-require */
import config from './config';


export function getClassName(characterClass) {
  if (characterClass === 'WIZARD') {
    return 'Відьмак';
  }
  if (characterClass === 'WARRIOR') {
    return 'Воїн';
  }
  if (characterClass === 'ROGUE') {
    return 'Шахрай';
  }
  return false;
}

export function getCharacterImage(characterSex, characterClass, characterImageStr) {
  return `${config.API_HOST}/media/portraits/${characterSex.toLowerCase()}/${characterClass.toLowerCase()}/${characterImageStr}.png`;
}

export function getMapImage(imageStr) {
  return `${config.API_HOST}/media/maps/${imageStr}.jpg`;
}

export function getEventImage(event) {
  if (event.eventType === 'QUESTCOMPLETE') {
    return getMapImage(event.imageStr);
  }
  return getCharacterImage(
    event.character.sexType.toLowerCase(),
    event.character.classType.toLowerCase(),
    event.character.imageStr,
  );
}

export function getEventTitle(event) {
  if (event.eventType === 'QUESTCOMPLETE') {
    return `Завдання ${event.quest.title} завершене!`;
  }
  if (event.eventType === 'LEVELUP') {
    return 'Покращення!';
  }
  if (event.eventType === 'DEATH') {
    return `${event.character.name} помирає`;
  }
  if (event.eventType === 'NEWMEMBER') {
    return 'Поповнення!';
  }
  return null;
}

export function getQuestType(questType) {
  if (questType === 'NEW') {
    return 'Нове';
  }
  if (questType === 'COMPLETED' || questType === 'CLOSED') {
    return 'Завершене';
  }
  if (questType === 'FAILED') {
    return 'Провалене';
  }
  if (questType === 'ACTIVE') {
    return 'Активне!';
  }
  return null;
}

export function getItemImage(imageStr) {
  return `${config.API_HOST}/media/items/${imageStr}.png`;
}

export function getItemDescription(item) {
  let ordinarity = '';
  let special = '';
  let description = '';
  if (item.itemRarity === 'RARE') {
    ordinarity = 'Незвичайна річ.';
  }
  if (item.itemRarity === 'MASTERPIECE') {
    ordinarity = 'Шедевр!';
  }
  if (item.itemRarity === 'LEGENDARY') {
    ordinarity = 'Легендарна річ!';
  }
  // WEAPON
  if (item.itemType === 'WEAPON') {
    special = `+${item.value} до пошкоджень.`;
  }
  // ARMOR
  if (item.itemType === 'ARMOR') {
    special = `+${item.value} до захисту.`;
  }
  // ACCURACY
  if (item.itemSpecial === 'ACCURACY') {
    special = `+${item.value} до влучності.`;
  }
  // QUIVER
  if (item.itemSpecial === 'QUIVER') {
    special = `+${item.value} до пошкоджень (використовуючи лук).`;
  }
  // POTION
  if (item.itemType === 'POTION') {
    // HEALTH
    if (item.itemSpecial === 'HEALTH') {
      special = `+${item.value} до самопочуття при вживанні.`;
    }
    // EXHAUSTION
    if (item.itemSpecial === 'EXHAUSTION') {
      special = `+${item.value} до відновлення витривалості при вживанні.`;
    }
  }
  // SUPPLY
  if (item.itemType === 'SUPPLY') {
    // EXHAUSTION
    if (item.itemSpecial === 'EXHAUSTION') {
      special = `+${item.value} до загального рівня витривалості.`;
    }
    // HEALTH
    if (item.itemSpecial === 'HEALTH') {
      special = `+${item.value} до загального рівня здоров'я.`;
    }
    // WEAPON
    if (item.itemSpecial === 'WEAPON') {
      special = `+${item.value} до пошкоджень.`;
    }
    // HEALTH
    if (item.itemSpecial === 'ARMOR') {
      special = `+${item.value} до захисту.`;
    }
    // HEALTH
    if (item.itemSpecial === 'ACCURACY') {
      special = `+${item.value} до влучності.`;
    }
    // FOOD
    if (item.itemSpecial === 'FOOD') {
      special = `+${item.value} до самопочуття при вживанні.`;
    }
  }
  description = `${special} ${ordinarity}`;
  description = description.trim();
  return description;
}
