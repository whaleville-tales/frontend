/*--------------------------------------------------------------------------------*/
/*                          Routes Import                                         */
/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/*                               Whales Page                                         */
/*--------------------------------------------------------------------------------*/
import Party from '../components/whale/Party';
import Guild from '../components/whale/Guild';
import Market from '../components/whale/makret/Market';
import Inventory from '../components/whale/inventory/Inventory';
import Cemetary from '../components/whale/Cemetary';
import Thanks from '../components/whale/Thanks';
import Quests from '../components/whale/quests/Quests';
import Events from '../components/whale/events/Events';
import Dashboard from '../components/whale/dashboard/Dashboard';


const ThemeRoutes = [
  {
    path: '/dashboard',
    name: 'Загальний огляд',
    icon: 'mdi mdi-view-dashboard',
    component: Dashboard,
  },
  {
    path: '/events',
    name: 'Журнал подій',
    icon: 'mdi mdi-map',
    component: Events,
  },
  {
    path: '/quests',
    name: 'Завдання',
    icon: 'mdi mdi-book',
    component: Quests,
  },
  {
    path: '/party',
    name: 'Гурт',
    icon: 'mdi mdi-account-multiple',
    component: Party,
  },
  {
    path: '/inventory',
    name: 'Скриня',
    icon: 'mdi mdi-treasure-chest',
    component: Inventory,
  },
  {
    path: '/market',
    name: 'Міський ринок',
    icon: 'mdi mdi-store',
    component: Market,
  },
  {
    path: '/guild',
    name: 'Гільдія найманців',
    icon: 'mdi mdi-account-plus',
    component: Guild,
  },
  {
    path: '/cemetary',
    name: 'Цвинтар',
    icon: 'mdi mdi-skull',
    component: Cemetary,
  },
  {
    path: '/thanks',
    name: 'Промінчики Любові',
    icon: 'mdi mdi-heart',
    component: Thanks,
  },
  {
    path: '/', pathTo: '/dashboard', name: 'Dashboard', redirect: true,
  },
];
export default ThemeRoutes;
