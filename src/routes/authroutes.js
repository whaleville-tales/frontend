import Login from '../components/auth/Login';
import Register from '../components/auth/Register';
import Recover from '../components/auth/Recover';

const authRoutes = [
  {
    path: '/authentication/login', name: 'Login', icon: 'mdi mdi-account-key', component: Login,
  },
  {
    path: '/authentication/register', name: 'Register', icon: 'mdi mdi-account-plus', component: Register,
  },
  {
    path: '/authentication/recover', name: 'Recover Password', icon: 'mdi mdi-account-convert', component: Recover,
  },
];
export default authRoutes;
