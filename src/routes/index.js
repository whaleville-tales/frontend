import Blanklayout from '../layout/blanklayout';
import WhaleLayout from '../layout/WhaleLayout';

const indexRoutes = [
  { path: '/authentication', name: 'Athentication', component: Blanklayout },
  { path: '/', name: 'Whale', component: WhaleLayout },
];

export default indexRoutes;
