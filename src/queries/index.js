import gql from 'graphql-tag';


export const LOGIN_MUTATION = gql`
    mutation($email: String!, $password: String!){
        tokenAuth(email: $email, password: $password){ token }
    }
`;

export const REGISTER_MUTATION = gql`
    mutation($email: String!, $password: String!){
        register(input:{
            email: $email
            password1: $password
            password2: $password
          })
          {
            response
          }
    }
`;

export const PARTY_QUERY = gql`
    query{
        characters(characterType:PARTY){
          uuid
          name
          sexType
          imageStr
          classType
          bio
          attributes{
            totalHp
            currentHp
            totalExhaustion
            currentExhaustion
            experience
            level{
              currentLevel
              nextLevelMaxExperience
              
            }
            armor
            damage
          }
          characteristics{
            strength
            dexterity
            constitution
            intelligence
            perception
          }
          revealedSkills{
            id
            title
            description
          }
        }
    }
`;

export const GUILD_QUERY = gql`
    query{
      characters(characterType:GUILD){
        uuid
        name
        sexType
        imageStr
        classType
        bio
        price
        attributes{
          totalHp
          currentHp
          totalExhaustion
          currentExhaustion
          experience
          level{
            currentLevel
            nextLevelMaxExperience
            
          }
          armor
          damage
        }
        characteristics{
          blurredStrength
          blurredDexterity
          blurredConstitution
          blurredIntelligence
          blurredPerception
        }
        revealedSkills{
          id
          title
          description
        }
        inventory{
          items{
          uuid
          title
          quantity
          value
          imageStr
          itemType
          itemRarity
          itemSpecial
          price
          }
        }
      }
    }
`;

export const HIRE_MUTATION = gql`
    mutation($uuid: String!){
      hireCharacter(input:{
        uuid:$uuid
      })
      {
        response
      }
    }
`;


export const CEMETARY_QUERY = gql`
    query{
      characters(characterType:DEAD){
        uuid
        name
        sexType
        imageStr
        classType
        death{
          deathReason
        }
      }
    }
`;


export const EVENTS_QUERY = gql`
    query{
      eventsAll{
        uuid
        description
        createdAt
        eventType
        imageStr
        character{
          name
          sexType
          classType
          imageStr
        }
        quest{
          title
        }
      }
    }
`;


export const QUEST_CHAINS_QUERY = gql`
    query{
      questChains{
        title
        uuid
        revealedQuests{
          uuid
          title
          questType
          description
          imageStr
        }
      }
    }
`;

export const MARKET_INVENTORIES_QUERY = gql`
    query{
      marketInventories{
        uuid
        inventoryType
        funds
          items{
          uuid
          title
          quantity
          value
          imageStr
          itemType
          itemRarity
          itemSpecial
          marketPrice
          price
        }
      }
    }
`;

export const TRANSFER_ITEM_MUTATION = gql`
    mutation($uuid: String!, $destinationInventoryUuid: String!, $order: Int!){
      transferItem(input:{
        uuid: $uuid
        destinationInventoryUuid: $destinationInventoryUuid
        order: $order
      })
      {
        response
      }
    }
`;

export const REORDER_ITEM_MUTATION = gql`
    mutation($uuid: String!, $order: Int!){
      reorderItem(input:{
        uuid: $uuid
        order: $order
      })
      {
        response
      }
    }
`;


export const PARTY_INVENTORIES_QUERY = gql`
    query{
      partyInventories{
        uuid
        currentSlots
        maxSlots
        owner{
          name
        }
        inventoryType
        funds
          items{
          uuid
          title
          quantity
          value
          imageStr
          itemType
          itemRarity
          itemSpecial
          marketPrice
          price
          order
        }
      }
    }
`;

export const IS_ADVENTURE_RUNNING_QUERY = gql`
    query{
        isAdventureRunning
    }
`;

export const LATEST_EVENT_QUERY = gql`
    query{
      latestEvent{
        uuid
        description
        imageStr
        eventType
      }
    }
`;

export const LATEST_QUEST_QUERY = gql`
    query{
      currentQuest{
        uuid
        title
        description
        imageStr
      }
    }
`;

export const LATEST_COMPLETED_QUEST_QUERY = gql`
    query{
      latestCompletedQuest{
        uuid
        title
        report
        imageStr
      }
    }
`;

export const LATEST_FAILED_QUEST_QUERY = gql`
    query{
      latestFailedQuest{
        uuid
        title
        imageStr
      }
    }
`;

export const PARTY_STATS_QUERY = gql`
    query{
        characters(characterType:PARTY){
          uuid
          name
          sexType
          imageStr
          classType
          attributes{
            totalHp
            currentHp
            totalExhaustion
            currentExhaustion
            experience
            level{
              nextLevelMaxExperience
            }
          }
        }
    }
`;


export const AVALIABLE_QUESTS_QUERY = gql`
    query{
      searchAvaliableQuests{
        title
        uuid
        description
        imageStr
        questType
        experienceValue
        rewardValue
      }
    }
`;

export const PENDING_QUESTS_QUERY = gql`
    query{
      searchPendingQuests {
        uuid
        title
        description
        imageStr
        questType
        experienceValue
        rewardValue
      }
    }
`;

export const FIND_QUEST_MUTATION = gql`
    mutation($uuid: String!){
      findQuest(input:{
        uuid: $uuid
      })
      {
        response
      }
    }
`;

export const START_QUEST_MUTATION = gql`
    mutation($uuid: String! $restart: Boolean){
      startQuest(input:{
        uuid: $uuid
        restart: $restart
      })
      {
        response
      }
    }
`;

export const CLOSE_QUEST_MUTATION = gql`
    mutation($uuid: String!){
      closeQuest(input:{
        uuid: $uuid
      })
      {
        response
      }
    }
`;

export const INITIAL_ACTION_LOG_QUERY = gql`
    query($skip: Int!){
      actionLog(skip: $skip){
        text
        logType
        id
        createdAt
      }
    }
`;

export const ACTION_LOG_SUBSCRIPTION = gql`
  subscription newLogSubscription($email: String!){
    newLogSubscription(channel: $email){
      actionLog{
        id
        text
        createdAt
      }
    }
  }
`;


export const USER_EMAIL_QUERY = gql`
    query{
        user{
          email
        }
      }
`;


export const RESTORE_PARTY_CHARACTERS_QUERY = gql`
    query{
      restorePartyCharacters
      }
`;

export const IS_GAME_FINISHED_QUERY = gql`
    query{
      isGameFinished
      }
`;

export const RESET_GAME_MUTATION = gql`
    mutation($resetGame: Boolean!){
      resetGame(input:{
        resetGame: $resetGame
      })
      {
        response
      }
    }
`;

export const GET_CURRENT_BACKGROUND_QUERY = gql`
    query{
      getCurrentBackground
      }
`;
