import React from 'react';
import {
  Card, CardBody, Col, Row, Progress,
} from 'reactstrap';
import PropTypes from 'prop-types';
import { getClassName, getCharacterImage } from '../../helpers/utils';
import Skill from './Skill';


function CharacterProfile(props) {
  const { character } = props;
  const imageUrl = getCharacterImage(
    character.sexType.toLowerCase(),
    character.classType.toLowerCase(),
    character.imageStr,
  );
  return (
    <Card>
      <CardBody className="little-profile text-center">
        <div className="pro-img">
          <img src={imageUrl} alt="" />
        </div>
        <h3 className="mb-0">{character.name}</h3>
        <h4><small>{getClassName(character.classType)}</small></h4>
        <p style={{ height: '100px' }}>
          {character.bio}
        </p>
        <div className="pt-2">
          <h4>
            <small>
              Самопочуття:
              {' '}
              {character.attributes.currentHp}
              /
              {character.attributes.totalHp}
            </small>

          </h4>
          <Progress className="mb-3" animated color="danger" value={(character.attributes.currentHp / character.attributes.totalHp) * 100} />
        </div>
        <div className="pt-2">
          <h4>
            <small>
              Виснаженість:
              {' '}
              {character.attributes.currentExhaustion}
              /
              {character.attributes.totalExhaustion}
            </small>

          </h4>
          <Progress className="mb-3" animated color="warning" value={(character.attributes.currentExhaustion / character.attributes.totalExhaustion) * 100} />
        </div>
        <div className="pt-2">
          <h4>
            <small>
              Досвід:
              {' '}
              {character.attributes.experience}
              /
              {character.attributes.level.nextLevelMaxExperience}
            </small>

          </h4>
          <Progress className="mb-3" animated value={(character.attributes.experience / character.attributes.level.nextLevelMaxExperience) * 100} />
        </div>
        <Row className="text-center mt-2">
          <Col lg="4" md="4" className="mt-2">
            <h3 className="mb-0 font-light">{character.attributes.level.currentLevel}</h3>
            <small>Рівень</small>
          </Col>
          <Col lg="4" md="4" className="mt-2">
            <h3 className="mb-0 font-light">{character.attributes.damage}</h3>
            <small>Атака</small>
          </Col>
          <Col lg="4" md="4" className="mt-2">
            <h3 className="mb-0 font-light">{character.attributes.armor}</h3>
            <small>Захист</small>
          </Col>
        </Row>
        <hr />
        <Row className="text-center mt-2">
          <Col lg="4" md="4" className="mt-2">
            <h3 className="mb-0 font-light">{character.characteristics.strength}</h3>
            <small>Сила</small>
          </Col>
          <Col lg="4" md="4" className="mt-2">
            <h3 className="mb-0 font-light">{character.characteristics.dexterity}</h3>
            <small>Кмітливість</small>
          </Col>
          <Col lg="4" md="4" className="mt-2">
            <h3 className="mb-0 font-light">{character.characteristics.constitution}</h3>
            <small>Тілобудова</small>
          </Col>
        </Row>
        <Row className="text-center mt-2">
          <Col lg="4" md="4" className="mt-2">
            <h3 className="mb-0 font-light">{character.characteristics.intelligence}</h3>
            <small>Інтелект</small>
          </Col>
          <Col lg="4" md="4" className="mt-2">
            <h3 className="mb-0 font-light">{character.characteristics.perception}</h3>
            <small>Чуття</small>
          </Col>
        </Row>
        <hr />
        <Row className="mt-2">
          <Col lg="12" md="12"><h4><small>Вміння</small></h4></Col>
          {character.revealedSkills.map(skill => <Skill key={skill.id} skill={skill} />)}
        </Row>
      </CardBody>
    </Card>
  );
}

CharacterProfile.propTypes = {
  character: PropTypes.objectOf(PropTypes.any),
};

export default CharacterProfile;
