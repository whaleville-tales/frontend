import React from 'react';
import {
  Card, CardBody, Col, Row, Button,
  Modal, ModalBody, ModalFooter,
} from 'reactstrap';
import PropTypes from 'prop-types';
import { Mutation } from 'react-apollo';
import { getClassName, getCharacterImage } from '../../helpers/utils';
import Skill from './Skill';
import Item from '../../components/whale/shared/Item';
import {
  HIRE_MUTATION, GUILD_QUERY, PARTY_QUERY, PARTY_INVENTORIES_QUERY,
  MARKET_INVENTORIES_QUERY,
} from '../../queries';


class GuildProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      serverError: '',
      modal: false,
    };
    this.handleHire = this.handleHire.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal,
    }));
  }

  handleHire(event, hireCharacter) {
    event.preventDefault();

    hireCharacter().then(async () => {
    }).catch((error) => {
      if (error.graphQLErrors) {
        this.setState({
          serverError: error.graphQLErrors.map(x => x.message),
          modal: true,
        });
      }
    });
  }

  render() {
    const { character } = this.props;
    const { serverError, modal } = this.state;
    const imageUrl = getCharacterImage(
      character.sexType.toLowerCase(),
      character.classType.toLowerCase(),
      character.imageStr,
    );
    const { items } = character.inventory[0];
    const { uuid } = character;
    let displayErrors;
    if (serverError) {
      displayErrors = serverError.map((element, key) => {
        let error = element;
        if (error.includes('Not enough funds')) {
          error = 'На жаль, у вас недостатньо коштів';
        }
        if (error.includes('Party is full')) {
          error = 'Ви не можете найняти більше людей';
        }
        const row = (
          <div
        // eslint-disable-next-line react/no-array-index-key
            key={key}
            className="error col-sm-12 text-center mb-3"
          >
            {error}

          </div>
        );
        return row;
      });
    }
    return (
      <Card>
        <CardBody className="little-profile text-center">
          <div className="pro-img">
            <img src={imageUrl} alt="user" />
          </div>
          <h3 className="mb-0">{character.name}</h3>
          <h4><small>{getClassName(character.classType)}</small></h4>
          <p style={{ height: '100px' }}>
            {character.bio}
          </p>
          <Row className="text-center mt-2">
            <Col lg="4" md="4" className="mt-2">
              <h3 className="mb-0 font-light">{character.attributes.level.currentLevel}</h3>
              <small>Рівень</small>
            </Col>
            <Col lg="4" md="4" className="mt-2">
              <h3 className="mb-0 font-light">{character.attributes.damage}</h3>
              <small>Атака</small>
            </Col>
            <Col lg="4" md="4" className="mt-2">
              <h3 className="mb-0 font-light">{character.attributes.armor}</h3>
              <small>Захист</small>
            </Col>
          </Row>
          <hr />
          <Row className="text-center mt-2">
            <Col lg="4" md="4" className="mt-2">
              <h3 className="mb-0 font-light">{character.characteristics.blurredStrength}</h3>
              <small>Сила</small>
            </Col>
            <Col lg="4" md="4" className="mt-2">
              <h3 className="mb-0 font-light">{character.characteristics.blurredDexterity}</h3>
              <small>Кмітливість</small>
            </Col>
            <Col lg="4" md="4" className="mt-2">
              <h3 className="mb-0 font-light">{character.characteristics.blurredConstitution}</h3>
              <small>Тілобудова</small>
            </Col>
          </Row>
          <Row className="text-center mt-2">
            <Col lg="4" md="4" className="mt-2">
              <h3 className="mb-0 font-light">{character.characteristics.blurredIntelligence}</h3>
              <small>Інтелект</small>
            </Col>
            <Col lg="4" md="4" className="mt-2">
              <h3 className="mb-0 font-light">{character.characteristics.blurredPerception}</h3>
              <small>Чуття</small>
            </Col>
          </Row>
          <hr />
          <Row className="mt-2 pb-2">
            <Col lg="12" md="12"><h4><small>Вміння</small></h4></Col>
            {character.revealedSkills.map(skill => <Skill key={skill.id} skill={skill} />)}
          </Row>
          <hr />
          <Row className="mt-2 pb-2">
            <Col lg="12" md="12"><h4><small>Речі</small></h4></Col>
            <Col lg="12" md="12" className="text-left pb-3">
              {items.map(item => <Item key={item.uuid} item={item} isMarket={false} />)}
            </Col>
          </Row>
          <Mutation
            mutation={HIRE_MUTATION}
            variables={{ uuid }}
            refetchQueries={[{ query: GUILD_QUERY }, { query: PARTY_QUERY },
              { query: PARTY_INVENTORIES_QUERY }, { query: MARKET_INVENTORIES_QUERY }]}
          >
            {hireCharacter => (
              <Button color="success" className="mt-2 btn btn-block" size="lg" onClick={event => this.handleHire(event, hireCharacter)}>
          Найняти за
                {' '}
                {' '}
                <span className="pr-1 ml-auto text-warning" style={{ fontWeight: 500 }}>{character.price}</span>
                <i className="mdi mdi-coins text-warning" />
              </Button>
            )}
          </Mutation>
          <Modal centered isOpen={modal} toggle={this.toggle}>
            <ModalBody>
              {displayErrors}
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={this.toggle}>Гаразд</Button>
            </ModalFooter>
          </Modal>
        </CardBody>
        )
      </Card>
    );
  }
}

GuildProfile.propTypes = {
  character: PropTypes.objectOf(PropTypes.any),
};

export default GuildProfile;
