import React from 'react';
import {
  Card, CardBody,
} from 'reactstrap';
import PropTypes from 'prop-types';
import { getClassName, getCharacterImage } from '../../helpers/utils';

function CemetaryProfile(props) {
  const { character } = props;
  const imageUrl = getCharacterImage(
    character.sexType.toLowerCase(),
    character.classType.toLowerCase(),
    character.imageStr,
  );
  return (
    <Card>
      <CardBody className="little-profile text-center">
        <div className="pro-img">
          <img src={imageUrl} alt="user" style={{ filter: 'grayscale(100%)' }} />
        </div>
        <h3 className="mb-0">{character.name}</h3>
        <h4><small>{getClassName(character.classType)}</small></h4>
        <div className="text-left">
          <p>
            {character.death.deathReason}
          </p>
        </div>
      </CardBody>
    </Card>
  );
}

CemetaryProfile.propTypes = {
  character: PropTypes.objectOf(PropTypes.any),
};

export default CemetaryProfile;
