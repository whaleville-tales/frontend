import React from 'react';
import { Col } from 'reactstrap';
import PropTypes from 'prop-types';

function Skill(props) {
  const { skill } = props;
  return (
    <Col lg="12" md="12" className="text-left pb-3" key={skill.id}>
      <div className="font-weight-bold">{skill.title}</div>
      {skill.description}
    </Col>
  );
}


Skill.propTypes = {
  skill: PropTypes.objectOf(PropTypes.string),
};

export default Skill;
