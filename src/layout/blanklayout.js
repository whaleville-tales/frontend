import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import authRoutes from '../routes/authroutes';


function Blanklayout() {
  return (
    <div className="authentications">
      <Switch>
        {authRoutes.map((prop, key) => {
          if (prop.redirect) {
            return (
              // eslint-disable-next-line react/no-array-index-key
              <Redirect from={prop.path} to={prop.pathTo} key={key} />
            );
          }
          return (
            // eslint-disable-next-line react/no-array-index-key
            <Route path={prop.path} component={prop.component} key={key} />
          );
        })}
      </Switch>
    </div>
  );
}
export default Blanklayout;
