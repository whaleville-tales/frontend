import React from 'react';
import { connect } from 'react-redux';
import {
  Nav,
  NavItem,
  NavLink,
  Navbar,
  NavbarBrand,
  Collapse,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import Cookies from 'js-cookie';
import PropTypes from 'prop-types';
import { Query, Mutation, withApollo } from 'react-apollo';
import {
  USER_EMAIL_QUERY, RESET_GAME_MUTATION,
  LATEST_EVENT_QUERY,
  AVALIABLE_QUESTS_QUERY, PENDING_QUESTS_QUERY,
  IS_ADVENTURE_RUNNING_QUERY, GUILD_QUERY,
  RESTORE_PARTY_CHARACTERS_QUERY, EVENTS_QUERY,
  CEMETARY_QUERY, PARTY_STATS_QUERY,
  PARTY_QUERY,
  PARTY_INVENTORIES_QUERY,
  MARKET_INVENTORIES_QUERY,
  GET_CURRENT_BACKGROUND_QUERY,
  LATEST_FAILED_QUEST_QUERY,
  INITIAL_ACTION_LOG_QUERY,
} from '../../queries';

/*--------------------------------------------------------------------------------*/
/* Import images which are need for the HEADER                                    */
/*--------------------------------------------------------------------------------*/
import whaleLogoSmall from '../../assets/images/whale-logo-white-small.png';

const mapStateToProps = state => ({
  ...state,
});

/*--------------------------------------------------------------------------------*/
/* To open SIDEBAR-MENU in MOBILE VIEW                                             */
/*--------------------------------------------------------------------------------*/
function showMobilemenu() {
  document.getElementById('main-wrapper').classList.toggle('show-sidebar');
}

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.sidebarHandler = this.sidebarHandler.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.clearLogCache = this.clearLogCache.bind(this);
    this.state = {
      isOpen: false,
      showResetMessage: false,
    };
  }

  sidebarHandler = () => {
    const { settings } = this.props;
    const element = document.getElementById('main-wrapper');
    switch (settings.activeSidebarType) {
      case 'full':
      case 'iconbar':
        element.classList.toggle('mini-sidebar');
        if (element.classList.contains('mini-sidebar')) {
          element.setAttribute('data-sidebartype', 'mini-sidebar');
        } else {
          element.setAttribute(
            'data-sidebartype',
            settings.activeSidebarType,
          );
        }
        break;

      case 'overlay':
      case 'mini-sidebar':
        element.classList.toggle('full');
        if (element.classList.contains('full')) {
          element.setAttribute('data-sidebartype', 'full');
        } else {
          element.setAttribute(
            'data-sidebartype',
            settings.activeSidebarType,
          );
        }
        break;

      default:
    }
  };

  handleReset(event, resetGame) {
    event.preventDefault();
    this.setState({
      showResetMessage: true,
    });
    resetGame().then(async () => {
      this.clearLogCache();
      this.setState({
        showResetMessage: false,
      });
    });
  }

  clearLogCache() {
    const { client } = this.props;
    client.writeQuery({
      query: INITIAL_ACTION_LOG_QUERY,
      variables: {
        skip: 0,
      },
      data: {
        actionLog: [],
      },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  handleLogout() {
    Cookies.remove('token');
  }

  /*--------------------------------------------------------------------------------*/
  /* To open NAVBAR in MOBILE VIEW                                                   */
  /*--------------------------------------------------------------------------------*/
  toggle() {
    const { isOpen } = this.state;
    this.setState({
      isOpen: !isOpen,
    });
  }

  render() {
    const { activeNavbarBg, settings } = this.props;
    const { isOpen, showResetMessage } = this.state;
    return (
      <header
        className="topbar navbarbg"
        data-navbarbg={activeNavbarBg}
      >
        <Navbar
          className={
            `top-navbar ${
              activeNavbarBg === 'skin6'
                ? 'navbar-light'
                : 'navbar-dark'}`
          }
          expand="md"
        >
          <div
            className="navbar-header"
            id="logobg"
            data-logobg="skin1"
          >
            {/*--------------------------------------------------------------------------------*/}
            {/* Mobile View Toggler  [visible only after 768px screen]                         */}
            {/*--------------------------------------------------------------------------------*/}
            <span
              role="button"
              tabIndex="0"
              className="nav-toggler d-block d-md-none text-white"
              onClick={showMobilemenu}
            >
              <i className="ti-menu ti-close" />
            </span>
            {/*--------------------------------------------------------------------------------*/}
            {/* Logos Or Icon will be goes here for Light Layout && Dark Layout                */}
            {/*--------------------------------------------------------------------------------*/}
            <NavbarBrand href="/">
              <b className="logo-icon">
                <img src={whaleLogoSmall} alt="homepage" className="dark-logo" />
                <img
                  src={whaleLogoSmall}
                  alt="homepage"
                  className="light-logo"
                />
              </b>
              <span className="logo-text">
            Whaleville tales
              </span>
            </NavbarBrand>
            {/*--------------------------------------------------------------------------------*/}
            {/* Mobile View Toggler  [visible only after 768px screen]                         */}
            {/*--------------------------------------------------------------------------------*/}
            <span
              role="button"
              tabIndex="0"
              className="topbartoggler d-block d-md-none text-white"
              onClick={this.toggle}
            >
              <i className="ti-more" />
            </span>
          </div>
          <Collapse
            className="navbarbg"
            isOpen={isOpen}
            navbar
            data-navbarbg={settings.activeNavbarBg}
          >
            <Nav className="float-left" navbar>
              <NavItem>
                <NavLink
                  href="#"
                  className="d-none d-md-block"
                  onClick={this.sidebarHandler}
                >
                  <i className="ti-menu" />
                </NavLink>
              </NavItem>
            </Nav>
            <div className="col-sm-6 text-right" style={{ color: 'white' }}>
              {showResetMessage && (<span>Скидаємо гру</span>)}
            </div>
            <Nav className="ml-auto float-right" navbar>
              {/*--------------------------------------------------------------------------------*/}
              {/* Start Profile Dropdown                                                         */}
              {/*--------------------------------------------------------------------------------*/}
              <UncontrolledDropdown nav inNavbar>

                <Query query={USER_EMAIL_QUERY}>
                  {({ data }) => (
                    <DropdownToggle nav caret>
                      {data && (<span style={{ fontSize: 'medium' }}>{data.user.email.split('@')[0]}</span>)}
                    </DropdownToggle>
                  )
                }
                </Query>
                <DropdownMenu right className="user-dd" style={{ minWidth: 'unset' }}>
                  <Mutation
                    mutation={RESET_GAME_MUTATION}
                    variables={{ resetGame: true }}
                    refetchQueries={[{ query: LATEST_EVENT_QUERY },
                      { query: AVALIABLE_QUESTS_QUERY },
                      { query: PENDING_QUESTS_QUERY }, { query: IS_ADVENTURE_RUNNING_QUERY },
                      { query: RESTORE_PARTY_CHARACTERS_QUERY }, { query: GUILD_QUERY },
                      { query: EVENTS_QUERY }, { query: CEMETARY_QUERY },
                      { query: LATEST_FAILED_QUEST_QUERY },
                      { query: PARTY_QUERY }, { query: MARKET_INVENTORIES_QUERY },
                      { query: PARTY_INVENTORIES_QUERY },
                      { query: GET_CURRENT_BACKGROUND_QUERY }, { query: PARTY_STATS_QUERY }]}
                  >
                    {resetGame => (
                      <DropdownItem onClick={event => this.handleReset(event, resetGame)}>
                        <i className="mdi mdi-restart mr-1 ml-1" />
                        {' '}
                        Скинути гру на початок
                      </DropdownItem>
                    )}
                  </Mutation>

                  <DropdownItem href="/authentication/login" onClick={() => this.handleLogout()}>
                    <i className="mdi mdi-power mr-1 ml-1" />
                    {' '}
                    Вийти
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              {/*--------------------------------------------------------------------------------*/}
              {/* End Profile Dropdown                                                           */}
              {/*--------------------------------------------------------------------------------*/}
            </Nav>
          </Collapse>
        </Navbar>
      </header>
    );
  }
}


Header.propTypes = {
  activeNavbarBg: PropTypes.string,
  settings: PropTypes.objectOf(PropTypes.any),
  client: PropTypes.objectOf(PropTypes.any),
};


export default withApollo(connect(mapStateToProps)(Header));
