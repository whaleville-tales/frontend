/* eslint-disable react/no-array-index-key */
/* eslint-disable no-shadow */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Nav,
  Collapse,
} from 'reactstrap';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Query } from 'react-apollo';
import { GET_CURRENT_BACKGROUND_QUERY } from '../../queries';
import { getMapImage } from '../../helpers/utils';


/*--------------------------------------------------------------------------------*/
/* To Expand SITE_LOGO With Sidebar-Menu on Hover                                  */
/*--------------------------------------------------------------------------------*/
function expandLogo() {
  document.getElementById('logobg').classList.toggle('expand-logo');
}

const mapStateToProps = state => ({
  ...state,
});

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
    this.state = {
      dashboard: this.activeRoute('/dahboard') !== '',
      dropdownOpen: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen,
      authentication: this.activeRoute('/authentication') !== '',
      dashboard: this.activeRoute('/dahboard') !== '',
      // dropdownOpen: false
    }));
  }

  /*--------------------------------------------------------------------------------*/
  /* Verifies if routeName is the one active (in browser input)                      */
  /*--------------------------------------------------------------------------------*/
  activeRoute(routeName) {
    const { location } = this.props;
    return location.pathname.indexOf(routeName) > -1 ? 'selected' : '';
  }

  render() {
    const { settings, routes } = this.props;
    return (
      <aside className="left-sidebar" id="sidebarbg" data-sidebarbg={settings.activeSidebarBg} onMouseEnter={expandLogo} onMouseLeave={expandLogo}>
        <div className="scroll-sidebar">
          <PerfectScrollbar className="sidebar-nav">
            {/*--------------------------------------------------------------------------------*/}
            {/* Profile Dropdown */}
            {/*--------------------------------------------------------------------------------*/}
            <Query query={GET_CURRENT_BACKGROUND_QUERY}>
              {({ data }) => (
                <div
                  className="user-profile row"
                  style={{
                    backgroundImage: `url(${data && getMapImage(data.getCurrentBackground)})`,
                    backgroundRepeat: 'no-repeat',
                  }}
                />
              )}
            </Query>

            {/*--------------------------------------------------------------------------------*/}
            {/* Sidebar Menus will go here                                                */}
            {/*--------------------------------------------------------------------------------*/}
            <Nav id="sidebarnav">
              {routes.map((prop, key) => {
                if (prop.redirect) {
                  return null;
                }
                if (prop.navlabel) {
                  return (
                    <li className="nav-small-cap" key={key}>
                      <i className={prop.icon} />
                      <span className="hide-menu">{prop.name}</span>
                    </li>
                  );
                }
                if (prop.collapse) {
                  const firstdd = {};
                  firstdd[prop.state] = !this.state[prop.state];
                  return (
                  /*---------------------------------------------------------------------------*/
                  /* Menus wiil be goes here                                                   */
                  /*---------------------------------------------------------------------------*/
                    // eslint-disable-next-line react/no-array-index-key
                    <li className={`${this.activeRoute(prop.path)} sidebar-item`} key={key}>
                      <span
                        role="button"
                        tabIndex="0"
                        data-toggle="collapse"
                        className="sidebar-link has-arrow"
                        aria-expanded={this.state[prop.state]}
                        onClick={() => this.setState(firstdd)}
                      >
                        <i className={prop.icon} />
                        <span className="hide-menu">{prop.name}</span>
                      </span>
                      {/*------------------------------------------------------------------------*/}
                      {/* Sub-Menus wiil be goes here                                            */}
                      {/*------------------------------------------------------------------------*/}
                      <Collapse
                        isOpen={this.state[prop.state]}
                      >
                        <ul className="first-level">
                          {prop.child.map((prop, key) => {
                            if (prop.redirect) return null;
                            if (prop.collapse) {
                              const seconddd = {};
                              seconddd[prop.state] = !this.state[prop.state];
                              return (
                                <li className={`${this.activeRoute(prop.path)} sidebar-item`} key={key}>
                                  <span
                                    role="button"
                                    tabIndex="0"
                                    data-toggle="collapse"
                                    className="sidebar-link has-arrow"
                                    aria-expanded={this.state[prop.state]}
                                    onClick={() => this.setState(seconddd)}
                                  >
                                    <i className={prop.icon} />
                                    <span className="hide-menu">{prop.name}</span>
                                  </span>
                                  {/*----------------------------------------------------*/}
                                  {/* Sub-Menus wiil be goes here                        */}
                                  {/*----------------------------------------------------*/}
                                  <Collapse isOpen={this.state[prop.state]}>
                                    <ul className="second-level">
                                      {prop.subchild.map((prop, key) => {
                                        if (prop.redirect) return null;
                                        return (
                                          <li className={`${this.activeRoute(prop.path)} sidebar-item`} key={key}>
                                            <NavLink to={prop.path} activeClassName="active" className="sidebar-link">
                                              <i className={prop.icon} />
                                              <span className="hide-menu">{prop.name}</span>
                                            </NavLink>
                                          </li>
                                        );
                                      })}
                                    </ul>
                                  </Collapse>
                                </li>
                              );
                            }
                            return (
                            /*--------------------------------------------------------*/
                            /* Adding Sidebar Item                                    */
                            /*--------------------------------------------------------*/
                              <li className={`${this.activeRoute(prop.path) + (prop.pro ? ' active active-pro' : '')} sidebar-item`} key={key}>
                                <NavLink to={prop.path} className="sidebar-link" activeClassName="active">
                                  <i className={prop.icon} />
                                  <span className="hide-menu">{prop.name}</span>
                                </NavLink>
                              </li>
                            );
                          })}
                        </ul>
                      </Collapse>
                    </li>
                  );
                }

                return (
                /*--------------------------------------------------------------------------------*/
                /* Adding Sidebar Item                                                            */
                /*--------------------------------------------------------------------------------*/
                  <li className={`${this.activeRoute(prop.path) + (prop.pro ? ' active active-pro' : '')} sidebar-item`} key={key}>
                    <NavLink to={prop.path} className="sidebar-link" activeClassName="active">
                      <i className={prop.icon} />
                      <span className="hide-menu">{prop.name}</span>
                    </NavLink>
                  </li>
                );
              })}
            </Nav>
          </PerfectScrollbar>
        </div>
      </aside>
    );
  }
}

Sidebar.propTypes = {
  location: PropTypes.objectOf(PropTypes.any),
  settings: PropTypes.objectOf(PropTypes.any),
  routes: PropTypes.arrayOf(PropTypes.any),
};


export default connect(mapStateToProps)(Sidebar);
