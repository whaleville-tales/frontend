/* eslint-disable react/no-array-index-key */
import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import Header from './header/header';
import Sidebar from './sidebar/sidebar';
import ThemeRoutes from '../routes/router';
import { GET_CURRENT_BACKGROUND_QUERY } from '../queries';
import { getMapImage } from '../helpers/utils';


const mapStateToProps = state => ({
  ...state,
});

class WhaleLayout extends React.Component {
  constructor(props) {
    super(props);
    this.updateDimensions = this.updateDimensions.bind(this);
    this.state = {
      width: window.innerWidth,
    };
    const { history } = this.props;

    history.listen(() => {
      if (window.innerWidth < 767
        && document.getElementById('main-wrapper').className.indexOf('show-sidebar') !== -1) {
        document.getElementById('main-wrapper').classList.toggle('show-sidebar');
      }
    });
  }

  /*--------------------------------------------------------------------------------*/
  /* Life Cycle Hook, Applies when loading or resizing App                           */
  /*--------------------------------------------------------------------------------*/
  componentDidMount() {
    window.addEventListener('load', this.updateDimensions);
    window.addEventListener('resize', this.updateDimensions);
  }

  /*--------------------------------------------------------------------------------*/
  /* Life Cycle Hook                                                                 */
  /*--------------------------------------------------------------------------------*/
  componentWillUnmount() {
    window.removeEventListener('load', this.updateDimensions);
    window.removeEventListener('resize', this.updateDimensions);
  }

  /*--------------------------------------------------------------------------------*/
  /* Function that handles sidebar, changes when resizing App                        */
  /*--------------------------------------------------------------------------------*/
  updateDimensions() {
    const { settings } = this.props;
    const { width } = this.state;
    const element = document.getElementById('main-wrapper');
    this.setState({
      width: window.innerWidth,
    });
    switch (settings.activeSidebarType) {
      case 'full':
      case 'iconbar':
        if (width < 1170) {
          element.setAttribute('data-sidebartype', 'mini-sidebar');
          element.classList.add('mini-sidebar');
        } else {
          element.setAttribute('data-sidebartype', settings.activeSidebarType);
          element.classList.remove('mini-sidebar');
        }
        break;

      case 'overlay':
        if (width < 767) {
          element.setAttribute('data-sidebartype', 'mini-sidebar');
        } else {
          element.setAttribute('data-sidebartype', settings.activeSidebarType);
        }
        break;

      default:
    }
  }


  render() {
    /*--------------------------------------------------------------------------------*/
    /* Theme Setting && Layout Options wiil be Change From Here                       */
    /*--------------------------------------------------------------------------------*/
    const { settings } = this.props;
    return (
      <div
        id="main-wrapper"
        dir={settings.activeDir}
        data-theme={settings.activeTheme}
        data-layout={settings.activeThemeLayout}
        data-sidebartype={settings.activeSidebarType}
        data-sidebar-position={settings.activeSidebarPos}
        data-header-position={settings.activeHeaderPos}
        data-boxed-layout={settings.activeLayout}
      >
        {/*--------------------------------------------------------------------------------*/}
        {/* Header                                                                         */}
        {/*--------------------------------------------------------------------------------*/}
        <Header />
        {/*--------------------------------------------------------------------------------*/}
        {/* Sidebar                                                                        */}
        {/*--------------------------------------------------------------------------------*/}
        <Sidebar {...this.props} routes={ThemeRoutes} />
        {/*--------------------------------------------------------------------------------*/}
        {/* Page Main-Content                                                              */}
        {/*--------------------------------------------------------------------------------*/}
        <div className="page-wrapper d-block">
          <Query query={GET_CURRENT_BACKGROUND_QUERY}>
            {({ data }) => (
              <div
                className="page-content container-fluid"
                style={{
                  backgroundImage: `url(${data && getMapImage(data.getCurrentBackground)})`,
                  backgroundRepeat: 'no-repeat',
                  backgroundAttachment: 'fixed',
                  backgroundSize: 'cover',
                  backgroundBlendMode: 'lighten',
                  backgroundColor: 'rgba(255,255,255,0.4)',
                }}
              >
                <Switch>
                  {ThemeRoutes.map((prop, key) => {
                    if (prop.navlabel) {
                      return null;
                    }
                    if (prop.collapse) {
                      return prop.child.map((prop2, key2) => {
                        if (prop2.collapse) {
                          return prop2.subchild.map((prop3, key3) => (
                            <Route path={prop3.path} component={prop3.component} key={key3} />
                          ));
                        }
                        return (
                          <Route path={prop2.path} component={prop2.component} key={key2} />
                        );
                      });
                    }
                    if (prop.redirect) {
                      return <Redirect from={prop.path} to={prop.pathTo} key={key} />;
                    }

                    return (
                      <Route path={prop.path} component={prop.component} key={key} />
                    );
                  })}
                </Switch>
              </div>
            )}
          </Query>

        </div>
      </div>
    );
  }
}


WhaleLayout.propTypes = {
  activeNavbarBg: PropTypes.string,
  activeLogoBg: PropTypes.string,
  history: PropTypes.objectOf(PropTypes.any),
  settings: PropTypes.objectOf(PropTypes.any),
};


export default connect(mapStateToProps)(WhaleLayout);
