import React from 'react';
import Loader from 'react-loader-spinner';


function LoadingSpinner() {
  return (
    <Loader
      type="Watch"
      color="#22abbd"
      height={25}
      width={25}
    />
  );
}


export default LoadingSpinner;
