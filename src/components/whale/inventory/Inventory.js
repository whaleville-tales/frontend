import React from 'react';
import {
  Card,
  Row,
  Col,
  CardBody,
  CardText,
} from 'reactstrap';
import { Query } from 'react-apollo';
import InventoryBoard from './InventoryBoard';
import { PARTY_INVENTORIES_QUERY } from '../../../queries';
import config from '../../../helpers/config';


function Inventory() {
  return (
    <div>
      <Row className="mx-auto">
        <Col sm="12">
          <Card>
            <CardBody className="">
              <CardText>
              Тут можна переглянути наявний інвентар і обмінятися речима.
              </CardText>
            </CardBody>
          </Card>
        </Col>
        <Col sm="12" className="mx-auto">
          <Query query={PARTY_INVENTORIES_QUERY} pollInterval={config.POLL_INTERVAL / 2}>
            {({ data, loading }) => {
              if (loading || !data) {
                return <div>Завантажується ...</div>;
              }
              return (
                <InventoryBoard
                  sharedInventoryOrigin={data.partyInventories[0]}
                  firstCharInventoryOrigin={data.partyInventories[1]}
                  secondCharInventoryOrigin={data.partyInventories[2]}
                  thirdCharInventoryOrigin={data.partyInventories[3]}
                />
              );
            }}
          </Query>
        </Col>
      </Row>
    </div>
  );
}

export default Inventory;
