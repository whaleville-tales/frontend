/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import PropTypes from 'prop-types';
import { withApollo } from 'react-apollo';
import InventoryDroppable from './InventoryDroppable';
import {
  TRANSFER_ITEM_MUTATION,
  REORDER_ITEM_MUTATION,
  PARTY_INVENTORIES_QUERY,
  MARKET_INVENTORIES_QUERY,
  PARTY_QUERY,
} from '../../../queries';

// data generator
function getItems(items) {
  return items.map(item => ({
    id: item.uuid,
    content: item,
  }));
}

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};


class InventoryBoard extends Component {
    /**
     * A semi-generic way to handle multiple lists. Matches
     * the IDs of the droppable container to the names of the
     * source arrays stored in the state.
     */
    id2List = {
      droppable1: 'sharedInventory',
      droppable2: 'firstCharInventory',
      droppable3: 'secondCharInventory',
      droppable4: 'thirdCharInventory',
    };

    constructor(props) {
      super(props);
      this.state = {
        firstCharInventory: null,
        secondCharInventory: null,
        thirdCharInventory: null,
        sharedInventory: null,
        firstCharInventoryId: '',
        secondCharInventoryId: '',
        thirdCharInventoryId: '',
        sharedInventoryId: '',
        sharedInventoryLoading: false,
        firstCharInventoryLoading: false,
        secondCharInventoryLoading: false,
        thirdCharInventoryLoading: false,
      };
    }

    componentDidMount() {
      const {
        firstCharInventoryOrigin, secondCharInventoryOrigin,
        thirdCharInventoryOrigin, sharedInventoryOrigin,
      } = this.props;
      if (firstCharInventoryOrigin) {
        this.setState({
          firstCharInventory: getItems(firstCharInventoryOrigin.items),
          firstCharInventoryId: firstCharInventoryOrigin.uuid,
        });
      }
      if (secondCharInventoryOrigin) {
        this.setState({
          secondCharInventory: getItems(secondCharInventoryOrigin.items),
          secondCharInventoryId: secondCharInventoryOrigin.uuid,
        });
      }
      if (thirdCharInventoryOrigin) {
        this.setState({
          thirdCharInventory: getItems(thirdCharInventoryOrigin.items),
          thirdCharInventoryId: thirdCharInventoryOrigin.uuid,
        });
      }
      this.setState({
        sharedInventory: getItems(sharedInventoryOrigin.items),
        sharedInventoryId: sharedInventoryOrigin.uuid,
      });
    }

    // eslint-disable-next-line react/destructuring-assignment
    getList = id => this.state[this.id2List[id]];

    onDragEnd = (result) => {
      const { source, destination } = result;
      const { client } = this.props;
      const {
        firstCharInventoryId,
        secondCharInventoryId,
        thirdCharInventoryId,
        sharedInventoryId,
      } = this.state;
      const itemId = result.draggableId;
      let itemOrderId;
      if (destination) {
        itemOrderId = destination.index;
      }

      // dropped outside the list
      if (!destination) {
        return;
      }

      if (source.droppableId === destination.droppableId) {
        const items = reorder(
          this.getList(source.droppableId),
          source.index,
          destination.index,
        );

        let state = { items };

        if (source.droppableId === 'droppable1') {
          state = { sharedInventory: items };
        }

        if (source.droppableId === 'droppable2') {
          state = { firstCharInventory: items };
        } else if (source.droppableId === 'droppable3') {
          state = { secondCharInventory: items };
        } else if (source.droppableId === 'droppable4') {
          state = { thirdCharInventory: items };
        }
        this.setState(state);

        client.mutate({
          mutation: REORDER_ITEM_MUTATION,
          variables: {
            uuid: itemId,
            order: itemOrderId,
          },
          refetchQueries: [{
            query: PARTY_INVENTORIES_QUERY,
          }, { query: MARKET_INVENTORIES_QUERY }],
        });
      } else {
        const result = move(
          this.getList(source.droppableId),
          this.getList(destination.droppableId),
          source,
          destination,
        );
        let destinationInventoryUuid = sharedInventoryId;
        this.setState({
          sharedInventoryLoading: true,
        });
        if (destination.droppableId === 'droppable2') {
          destinationInventoryUuid = firstCharInventoryId;
          this.setState({
            firstCharInventoryLoading: true,
            sharedInventoryLoading: false,
          });
        }
        if (destination.droppableId === 'droppable3') {
          destinationInventoryUuid = secondCharInventoryId;
          this.setState({
            secondCharInventoryLoading: true,
            sharedInventoryLoading: false,
          });
        }
        if (destination.droppableId === 'droppable4') {
          destinationInventoryUuid = thirdCharInventoryId;
          this.setState({
            thirdCharInventoryLoading: true,
            sharedInventoryLoading: false,
          });
        }

        if (result.droppable1) {
          this.setState({
            sharedInventory: result.droppable1,
          });
        }
        if (result.droppable2) {
          this.setState({
            firstCharInventory: result.droppable2,
          });
        }
        if (result.droppable3) {
          this.setState({
            secondCharInventory: result.droppable3,
          });
        }
        if (result.droppable4) {
          this.setState({
            thirdCharInventory: result.droppable4,
          });
        }

        client.mutate({
          mutation: TRANSFER_ITEM_MUTATION,
          variables: {
            uuid: itemId,
            destinationInventoryUuid,
            order: itemOrderId,
          },
          refetchQueries: [{
            query: PARTY_INVENTORIES_QUERY,
          },
          {
            query: PARTY_QUERY,
          },
          { query: MARKET_INVENTORIES_QUERY }],
        }).then(async () => {
          this.setState({
            sharedInventoryLoading: false,
            firstCharInventoryLoading: false,
            secondCharInventoryLoading: false,
            thirdCharInventoryLoading: false,
          });
        });
      }
    };

    render() {
      const {
        sharedInventory, firstCharInventory, secondCharInventory, thirdCharInventory,
        sharedInventoryLoading, firstCharInventoryLoading,
        secondCharInventoryLoading, thirdCharInventoryLoading,
      } = this.state;
      const {
        firstCharInventoryOrigin, secondCharInventoryOrigin,
        thirdCharInventoryOrigin, sharedInventoryOrigin,
      } = this.props;
      return (
        <DragDropContext onDragEnd={this.onDragEnd}>
          {sharedInventory && (
          <div className="row mx-auto">
            <div className="col-3">
              <InventoryDroppable
                droppableId="droppable1"
                name="Скриня"
                funds={sharedInventoryOrigin.funds}
                items={sharedInventory}
                loading={sharedInventoryLoading}
              />
            </div>
            {firstCharInventory && (
            <div className="col-3">
              <InventoryDroppable
                droppableId="droppable2"
                name={firstCharInventoryOrigin.owner.name}
                currentSlots={firstCharInventoryOrigin.currentSlots}
                totalSlots={firstCharInventoryOrigin.maxSlots}
                items={firstCharInventory}
                loading={firstCharInventoryLoading}
              />
            </div>
            )}
            {secondCharInventory && (
            <div className="col-3">
              <InventoryDroppable
                droppableId="droppable3"
                name={secondCharInventoryOrigin.owner.name}
                currentSlots={secondCharInventoryOrigin.currentSlots}
                totalSlots={secondCharInventoryOrigin.maxSlots}
                items={secondCharInventory}
                loading={secondCharInventoryLoading}
              />
            </div>
            )}
            {thirdCharInventory && (
            <div className="col-3">
              <InventoryDroppable
                droppableId="droppable4"
                name={thirdCharInventoryOrigin.owner.name}
                currentSlots={thirdCharInventoryOrigin.currentSlots}
                totalSlots={thirdCharInventoryOrigin.maxSlots}
                items={thirdCharInventory}
                loading={thirdCharInventoryLoading}
              />
            </div>
            )}

          </div>
          )}

        </DragDropContext>
      );
    }
}

InventoryBoard.propTypes = {
  firstCharInventoryOrigin: PropTypes.objectOf(PropTypes.any),
  secondCharInventoryOrigin: PropTypes.objectOf(PropTypes.any),
  thirdCharInventoryOrigin: PropTypes.objectOf(PropTypes.any),
  sharedInventoryOrigin: PropTypes.objectOf(PropTypes.any),
  client: PropTypes.objectOf(PropTypes.any),
};

export default withApollo(InventoryBoard);
