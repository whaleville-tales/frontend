/* eslint-disable no-shadow */
import React from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import PropTypes from 'prop-types';
import {
  Card, CardBody, CardTitle, Row, Col,
} from 'reactstrap';
import Item from '../shared/Item';
import LoadingSpinner from '../../../layout/loadingSpinner/LoadingSpinner';


function InventoryDroppable(props) {
  const {
    droppableId, name, currentSlots, totalSlots, items, funds, loading,
  } = props;
  return (
    <Droppable droppableId={droppableId}>
      {provided => (
        <Card>
          <CardBody>
            <Row>
              <Col sm="9">
                <CardTitle>
                  {name}
                  {' '}
                  {funds ? (
                    <div>
                        є
                      {' '}
                      {' '}
                      <span className="pr-1 ml-auto text-warning">{funds}</span>
                      <i className="mdi mdi-coins text-warning" />
                    </div>
                  )
                    : (
                      <div>
                        {currentSlots}
                          /
                        {totalSlots}
                        {' '}
                          речей
                      </div>
                    )
                      }
                </CardTitle>
              </Col>
              <Col sm="3" className="text-right">
                {loading && (<LoadingSpinner />)}
              </Col>
            </Row>
            <div
              ref={provided.innerRef}
              className="feeds"
            >
              {items.length > 0 ? (
                items.map((item, index) => (
                  <Draggable
                    key={item.id}
                    draggableId={item.id}
                    index={index}
                  >
                    {provided => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <Item item={item.content} isMarket={false} />
                        {provided.placeholder}
                      </div>
                    )}
                  </Draggable>
                ))
              ) : (
                <div ref={provided.innerRef} {...provided.droppableProps}>
                              Порожньо
                  {provided.placeholder}
                </div>
              )}
            </div>
          </CardBody>
        </Card>
      )}
    </Droppable>
  );
}

InventoryDroppable.propTypes = {
  droppableId: PropTypes.string,
  name: PropTypes.string,
  funds: PropTypes.number,
  currentSlots: PropTypes.number,
  totalSlots: PropTypes.number,
  items: PropTypes.arrayOf(PropTypes.any),
  loading: PropTypes.bool,
};

export default InventoryDroppable;
