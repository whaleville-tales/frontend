import React from 'react';
import {
  Card,
  Row,
  Col,
  CardBody,
  CardText,
} from 'reactstrap';
import { Query } from 'react-apollo';
import TradeBoard from './TradeBoard';
import { MARKET_INVENTORIES_QUERY } from '../../../queries';
import config from '../../../helpers/config';


function Market() {
  return (
    <div>
      <Row className="mx-auto">
        <Col sm="12">
          <Card>
            <CardBody className="">
              <CardText>
              Тут можна придбати чи продати будь який крам, зібраний з багатих навколишніх країв.
              </CardText>
            </CardBody>
          </Card>
        </Col>
        <Query query={MARKET_INVENTORIES_QUERY} pollInterval={config.POLL_INTERVAL}>
          {({ data, loading }) => {
            if (loading || !data) {
              return <div>Завантажується ...</div>;
            }
            return (
              <Col sm="10" className="mx-auto">
                <TradeBoard
                  market={data.marketInventories[0]}
                  shared={data.marketInventories[1]}
                />
              </Col>
            );
          }}
        </Query>

      </Row>
    </div>
  );
}

export default Market;
