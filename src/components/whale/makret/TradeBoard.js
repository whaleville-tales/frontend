import React, { Component } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import {
  Card, CardBody, CardTitle, Row, Col, Modal, ModalBody, ModalFooter, Button,
} from 'reactstrap';
import PropTypes from 'prop-types';
import { withApollo } from 'react-apollo';
import Item from '../shared/Item';
import LoadingSpinner from '../../../layout/loadingSpinner/LoadingSpinner';
import {
  TRANSFER_ITEM_MUTATION,
  REORDER_ITEM_MUTATION,
  MARKET_INVENTORIES_QUERY,
  PARTY_INVENTORIES_QUERY,
} from '../../../queries';

// data generator
function getItems(items) {
  return items.map(item => ({
    id: item.uuid,
    content: item,
  }));
}

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};


class TradeBoard extends Component {
  /**
     * A semi-generic way to handle multiple lists. Matches
     * the IDs of the droppable container to the names of the
     * source arrays stored in the state.
     */
    id2List = {
      market: 'items',
      shared: 'selected',
    };

    constructor(props) {
      super(props);
      const { market, shared } = this.props;
      this.state = {
        marketId: market.uuid,
        sharedId: shared.uuid,
        items: getItems(market.items),
        selected: getItems(shared.items),
        sharedFunds: shared.funds,
        serverError: '',
        modal: false,
        loading: false,
      };
      this.toggle = this.toggle.bind(this);
    }

    // eslint-disable-next-line react/destructuring-assignment
    getList = id => this.state[this.id2List[id]];

    onDragEnd = (result) => {
      const { client } = this.props;
      const { source, destination } = result;
      const { marketId, sharedId } = this.state;
      const itemId = result.draggableId;
      let itemOrderId;
      if (destination) {
        itemOrderId = destination.index;
      }

      // dropped outside the list
      if (!destination) {
        return;
      }

      if (source.droppableId === destination.droppableId) {
        const items = reorder(
          this.getList(source.droppableId),
          source.index,
          destination.index,
        );

        let state = { items };

        if (source.droppableId === 'shared') {
          state = { selected: items };
        }
        this.setState(state);


        client.mutate({
          mutation: REORDER_ITEM_MUTATION,
          variables: {
            uuid: itemId,
            order: itemOrderId,
          },
          refetchQueries: [{
            query: MARKET_INVENTORIES_QUERY,
          }, { query: PARTY_INVENTORIES_QUERY }],
        });
      } else {
        // ITEM MOVED TO ANOTHER LIST
        let destinationInventoryUuid = sharedId;
        if (destination.droppableId === 'market') {
          destinationInventoryUuid = marketId;
        }
        // eslint-disable-next-line no-shadow
        const result = move(
          this.getList(source.droppableId),
          this.getList(destination.droppableId),
          source,
          destination,
        );

        this.setState({
          items: result.market,
          selected: result.shared,
          loading: true,
        });

        client.mutate({
          mutation: TRANSFER_ITEM_MUTATION,
          variables: {
            uuid: itemId,
            destinationInventoryUuid,
            order: itemOrderId,
          },
          refetchQueries: [{
            query: MARKET_INVENTORIES_QUERY,
          }, { query: PARTY_INVENTORIES_QUERY }],
        }).then(async () => {
          this.setState({
            loading: false,
          });
        }).catch((error) => {
          this.setState({
            serverError: error.graphQLErrors.map(x => x.message),
            modal: true,
          });
        });
      }
    };

    toggle() {
      // Restore inventory state on error
      const { market, shared } = this.props;
      this.setState(prevState => ({
        modal: !prevState.modal,
        items: getItems(market.items),
        selected: getItems(shared.items),
      }));
    }

    render() {
      const {
        items, selected, serverError, modal, loading,
      } = this.state;
      const { shared } = this.props;

      let displayErrors;
      if (serverError) {
        displayErrors = serverError.map((element, key) => {
          let error = element;
          if (error.includes('Not enough funds')) {
            error = 'На жаль, у вас недостатньо коштів';
          }
          const row = (
            <div
        // eslint-disable-next-line react/no-array-index-key
              key={key}
              className="error col-sm-12 text-center mb-3"
            >
              {error}

            </div>
          );
          return row;
        });
      }

      return (
        <DragDropContext onDragEnd={this.onDragEnd}>
          <div className="row mx-auto">
            <div className="col-6">
              <Droppable droppableId="market">
                {provided => (
                  <Card>
                    <CardBody>
                      <Row>
                        <Col sm="6"><CardTitle>Ринок</CardTitle></Col>
                        <Col sm="6" className="text-right">
                          {loading && (<LoadingSpinner />)}
                        </Col>
                      </Row>
                      <div
                        ref={provided.innerRef}
                        className="feeds"
                      >
                        {items.length > 0 ? (
                          items.map((item, index) => (
                            <Draggable
                              key={item.id}
                              draggableId={item.id}
                              index={index}
                            >
                              {innerProvided => (
                                <div
                                  ref={innerProvided.innerRef}
                                  {...innerProvided.draggableProps}
                                  {...innerProvided.dragHandleProps}
                                >
                                  <Item item={item.content} isMarket />
                                </div>
                              )}
                            </Draggable>
                          ))
                        ) : (
                          <div ref={provided.innerRef} {...provided.droppableProps}>
                              Порожньо
                            {provided.placeholder}
                          </div>
                        )}
                        {provided.placeholder}
                      </div>
                    </CardBody>
                  </Card>
                )}
              </Droppable>
            </div>
            <div className="col-6">
              <Droppable droppableId="shared" height={2000} style={{ minHeight: '2000px' }} ignoreContainerClipping>
                {provided => (
                  <Card>
                    <CardBody>
                      <Row>
                        <Col sm="6"><CardTitle>Скриня</CardTitle></Col>
                        <Col sm="6" className="text-right">
                          <CardTitle>
                            є
                            {' '}
                            <span className="pr-1 ml-auto text-warning">{shared.funds}</span>
                            <i className="mdi mdi-coins text-warning" />
                          </CardTitle>
                        </Col>
                      </Row>
                      <div
                        ref={provided.innerRef}
                        className="feeds"
                      >
                        {selected.length > 0 ? (
                          selected.map((item, index) => (
                            <Draggable
                              key={item.id}
                              draggableId={item.id}
                              index={index}
                            >
                              {innerProvided => (
                                <div
                                  ref={innerProvided.innerRef}
                                  {...innerProvided.draggableProps}
                                  {...innerProvided.dragHandleProps}
                                >
                                  <Item item={item.content} isMarket={false} />
                                </div>
                              )}
                            </Draggable>
                          ))
                        ) : (
                          <div ref={provided.innerRef} {...provided.droppableProps}>
                              Порожньо
                            {provided.placeholder}
                          </div>
                        )}

                        {provided.placeholder}
                      </div>
                    </CardBody>
                  </Card>
                )}
              </Droppable>
            </div>
          </div>
          <Modal centered isOpen={modal} toggle={this.toggle}>
            <ModalBody>
              {displayErrors}
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={this.toggle}>Гаразд</Button>
            </ModalFooter>
          </Modal>
        </DragDropContext>
      );
    }
}

TradeBoard.propTypes = {
  market: PropTypes.objectOf(PropTypes.any),
  shared: PropTypes.objectOf(PropTypes.any),
  client: PropTypes.objectOf(PropTypes.any),
};

export default withApollo(TradeBoard);
