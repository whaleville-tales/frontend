import React from 'react';
import { Progress } from 'reactstrap';
import PropTypes from 'prop-types';


function StatsItem(props) {
  const {
    image, username, smtext, hitsCurrent, hitsMax, expCurrent, expMax,
    exhaustionCurrent, exhaustionMax,
  } = props;

  return (
    <tr>
      <td>
        <span className="round"><img src={image} alt="" width="50" /></span>
      </td>
      <td>
        <h6 className="font-medium mb-0">{username}</h6>
        <small className="text-muted">{smtext}</small>
      </td>
      <td>
        <div className="pt-2">
          <h6 className="font-medium mb-0 text-center">
            {hitsCurrent}
            /
            {hitsMax}
          </h6>
          <Progress className="mb-3" animated color="danger" value={(hitsCurrent / hitsMax) * 100} />
        </div>
      </td>
      <td>
        <div className="pt-2">
          <h6 className="font-medium mb-0 text-center">
            {exhaustionCurrent}
            /
            {exhaustionMax}
          </h6>
          <Progress className="mb-3" animated color="warning" value={(exhaustionCurrent / exhaustionMax) * 100} />
        </div>
      </td>
      <td>
        <div className="pt-2">
          <h6 className="font-medium mb-0 text-center">
            {expCurrent}
            /
            {expMax}
          </h6>
          <Progress className="mb-3" animated value={(expCurrent / expMax) * 100} />
        </div>
      </td>
    </tr>
  );
}


StatsItem.propTypes = {
  image: PropTypes.string,
  username: PropTypes.string,
  smtext: PropTypes.string,
  hitsCurrent: PropTypes.number,
  hitsMax: PropTypes.number,
  expCurrent: PropTypes.number,
  expMax: PropTypes.number,
  exhaustionCurrent: PropTypes.number,
  exhaustionMax: PropTypes.number,
};

export default StatsItem;
