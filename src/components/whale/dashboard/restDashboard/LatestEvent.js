import React from 'react';
import {
  Card,
  CardBody,
  CardSubtitle,
  CardText,
  CardImg,
} from 'reactstrap';
import PropTypes from 'prop-types';


function LatestEvent(props) {
  const {
    image, body,
  } = props;
  return (
    <Card>
      <CardBody style={{ marginBottom: '-10px' }}>
        <CardSubtitle>Останні події</CardSubtitle>
      </CardBody>
      <CardImg top width="100%" src={image} />
      <CardBody>
        <CardText>{body}</CardText>
      </CardBody>
    </Card>
  );
}


LatestEvent.propTypes = {
  body: PropTypes.string,
  image: PropTypes.string,
};


export default LatestEvent;
