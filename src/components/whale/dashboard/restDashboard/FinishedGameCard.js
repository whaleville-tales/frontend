import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Button,
} from 'reactstrap';
import { Mutation } from 'react-apollo';
import {
  RESET_GAME_MUTATION,
  LATEST_EVENT_QUERY,
  AVALIABLE_QUESTS_QUERY, PENDING_QUESTS_QUERY,
  IS_ADVENTURE_RUNNING_QUERY, GUILD_QUERY,
  RESTORE_PARTY_CHARACTERS_QUERY, EVENTS_QUERY,
  CEMETARY_QUERY, IS_GAME_FINISHED_QUERY,
  GET_CURRENT_BACKGROUND_QUERY,
  PARTY_STATS_QUERY,
} from '../../../../queries';


class FinishedGameCard extends React.Component {
  constructor(props) {
    super(props);
    this.handleReset = this.handleReset.bind(this);
  }

  // eslint-disable-next-line class-methods-use-this
  handleReset(event, resetGame) {
    event.preventDefault();
    resetGame();
  }

  render() {
    return (
      <Card>
        <CardBody>
          <CardTitle>Вітаємо! Ви змогли вберегти Китове!</CardTitle>
          <CardText>
            Ви зробили це попри всі потуги бандитів-націоналістів,
            уражених грибком мертвотних житців та навіть орди почвар з морської пучини.
          </CardText>
          <CardText>
            Якою б не була ваша подальша доля, в
            Китовому та навколишніх землях ще довгі століття оспівуватимуть ваші подвиги.
          </CardText>
          <CardText>
            Жителі Китового вирішили назвати своє місто у вашу честь.
          </CardText>
          <Mutation
            mutation={RESET_GAME_MUTATION}
            variables={{ resetGame: true }}
            refetchQueries={[{ query: LATEST_EVENT_QUERY },
              { query: AVALIABLE_QUESTS_QUERY }, { query: PARTY_STATS_QUERY },
              { query: PENDING_QUESTS_QUERY }, { query: IS_ADVENTURE_RUNNING_QUERY },
              { query: RESTORE_PARTY_CHARACTERS_QUERY }, { query: GUILD_QUERY },
              { query: EVENTS_QUERY }, { query: CEMETARY_QUERY },
              { query: IS_GAME_FINISHED_QUERY }, { query: GET_CURRENT_BACKGROUND_QUERY }]}
          >
            {resetGame => (
              <Button color="success" className="mt-2 btn btn-block" size="lg" onClick={event => this.handleReset(event, resetGame)}>
                  Зайти на коло
              </Button>
            )}
          </Mutation>
        </CardBody>
      </Card>
    );
  }
}


export default FinishedGameCard;
