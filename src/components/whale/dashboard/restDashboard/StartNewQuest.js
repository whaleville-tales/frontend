import React from 'react';
import {
  Button, Modal, ModalBody, ModalFooter,
  Card, CardBody, CardTitle, CardSubtitle, CardImg, CardText,
} from 'reactstrap';
import { Mutation } from 'react-apollo';
import PropTypes from 'prop-types';
import { getMapImage } from '../../../../helpers/utils';
import {
  START_QUEST_MUTATION, IS_ADVENTURE_RUNNING_QUERY,
  LATEST_QUEST_QUERY, INITIAL_ACTION_LOG_QUERY,
  GET_CURRENT_BACKGROUND_QUERY,
} from '../../../../queries';


class StartNewQuest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };
    this.handleStart = this.handleStart.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal,
    }));
  }

  handleStart(event, startQuest) {
    event.preventDefault();
    startQuest().then(async () => {
      this.toggle();
    });
  }

  render() {
    const { quests } = this.props;
    const { modal } = this.state;
    return (
      <div style={{ textAlign: 'center' }}>
        <Button color="primary" className="mt-2 btn btn-block" size="lg" onClick={this.toggle}>Обрати наступне завдання</Button>
        <Modal centered isOpen={modal} toggle={this.toggle}>
          <ModalBody>
            {quests.length ? quests.map(quest => (
              <Card key={quest.uuid}>
                <CardImg top width="100%" src={getMapImage(quest.imageStr)} alt="" />
                <CardBody>
                  <CardTitle>{quest.title}</CardTitle>
                  <CardSubtitle>
                    {' '}
                    Нагорода:
                    {' '}
                    {quest.rewardValue}
                    {' '}
                    <i className="mdi mdi-coins text-warning" />
                    ,
                    {' '}
                    {quest.experienceValue}
                    {' '}
                    пунктів досвіду
                  </CardSubtitle>
                  <CardText>{quest.description}</CardText>
                  <Mutation
                    mutation={START_QUEST_MUTATION}
                    variables={{ uuid: quest.uuid }}
                    refetchQueries={[{ query: IS_ADVENTURE_RUNNING_QUERY },
                      { query: LATEST_QUEST_QUERY }, { query: INITIAL_ACTION_LOG_QUERY },
                      { query: GET_CURRENT_BACKGROUND_QUERY }]}
                  >
                    {startQuest => (
                      <Button color="primary" onClick={event => this.handleStart(event, startQuest)}>Почати</Button>
                    )}
                  </Mutation>
                </CardBody>
              </Card>
            )) : (
              <p>Наразі ви виконали усі завдання зі списку. Спробуйте пошукати нові. </p>
            )}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>Закрити</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

StartNewQuest.propTypes = {
  quests: PropTypes.arrayOf(PropTypes.any),
};

export default StartNewQuest;
