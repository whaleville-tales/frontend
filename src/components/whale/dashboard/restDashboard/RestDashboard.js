/* eslint-disable no-shadow */
import React from 'react';
import {
  Alert,
  Row,
  Col,
  Card,
  CardBody, CardText,
} from 'reactstrap';
import { Query } from 'react-apollo';
import LatestEvent from './LatestEvent';
import Stats from '../Stats';

import {
  LATEST_EVENT_QUERY, PARTY_STATS_QUERY,
  AVALIABLE_QUESTS_QUERY, PENDING_QUESTS_QUERY,
  LATEST_FAILED_QUEST_QUERY, IS_GAME_FINISHED_QUERY,
} from '../../../../queries';
import StartNewQuest from './StartNewQuest';
import SearchNewQuests from './SearchNewQuests';
import { getMapImage } from '../../../../helpers/utils';
import LatestFailedQuest from './LatestFailedQuest';
import FinishedGameCard from './FinishedGameCard';
import config from '../../../../helpers/config';

function RestDashboard() {
  return (

    <Query query={IS_GAME_FINISHED_QUERY}>
      {({ data }) => (
        data && data.isGameFinished ? (
          <Row className="mx-auto">
            <Col sm="12">
              <FinishedGameCard />
            </Col>
          </Row>
        ) : (
          <Row className="mx-auto">
            <Col sm="6">
              <Query query={LATEST_FAILED_QUEST_QUERY} pollInterval={config.POLL_INTERVAL}>
                {({ data }) => (
                  <div>
                    {data && data.latestFailedQuest ? (
                      <LatestFailedQuest
                        uuid={data.latestFailedQuest.uuid}
                        title={data.latestFailedQuest.title}
                        image={getMapImage(data.latestFailedQuest.imageStr)}
                      />
                    ) : (
                      <Query query={LATEST_EVENT_QUERY}>
                        {({ data, loading }) => {
                          if (loading || !data) {
                            return <div>Завантажується ...</div>;
                          }
                          return (
                            <Row>
                              {data.latestEvent ? (
                                <LatestEvent
                                  body={data.latestEvent.description}
                                  image={getMapImage(data.latestEvent.imageStr)}
                                />
                              ) : (
                                <Card className="col-sm-12">
                                  <CardBody>
                                    <CardText>
                                    Незабаром на цьому екрані зʼявиться багато інформації,
                                      але наразі вам потрібно дістатися до Китового!
                                    </CardText>
                                    <Alert color="primary">
                                    Підказка: розпочніть власні пригоди натиснувши кнопку
                                    &quot;Обрати наступне завдання&quot;!
                                    </Alert>
                                  </CardBody>
                                </Card>
                              )}
                            </Row>
                          );
                        }}
                      </Query>
                    )}
                  </div>
                )}
              </Query>
            </Col>
            <Col sm="6">
              <Query query={PARTY_STATS_QUERY} pollInterval={config.POLL_INTERVAL}>
                {({ data, loading }) => {
                  if (loading || !data) {
                    return <div>Завантажується ...</div>;
                  }
                  return (
                    <div>
                      {data.characters && (
                        <Stats
                          characters={data.characters}
                        />
                      )}
                    </div>
                  );
                }}
              </Query>
              <Card>
                <CardBody>
                  <Query query={AVALIABLE_QUESTS_QUERY}>
                    {({ data, loading }) => {
                      if (loading || !data) {
                        return <div>Завантажується ...</div>;
                      }
                      return (
                        <StartNewQuest quests={data.searchAvaliableQuests} />
                      );
                    }}
                  </Query>
                </CardBody>
              </Card>
              <Card>
                <CardBody>
                  <Query query={PENDING_QUESTS_QUERY}>
                    {({ data, loading }) => {
                      if (loading || !data) {
                        return <div>Завантажується ...</div>;
                      }
                      return (
                        <SearchNewQuests quests={data.searchPendingQuests} />
                      );
                    }}
                  </Query>
                </CardBody>
              </Card>
            </Col>
          </Row>
        )
      )}
    </Query>
  );
}

export default RestDashboard;
