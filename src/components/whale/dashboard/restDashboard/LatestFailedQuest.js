import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Button,
  CardImg,
} from 'reactstrap';
import { Mutation, withApollo } from 'react-apollo';
import PropTypes from 'prop-types';
import {
  START_QUEST_MUTATION, IS_ADVENTURE_RUNNING_QUERY,
  LATEST_QUEST_QUERY, INITIAL_ACTION_LOG_QUERY,
  GET_CURRENT_BACKGROUND_QUERY,
} from '../../../../queries';


class LatestFailedQuest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.handleFail = this.handleFail.bind(this);
    this.clearLogCache = this.clearLogCache.bind(this);
  }

  handleFail(event, restartQuest) {
    event.preventDefault();
    this.clearLogCache();
    restartQuest();
  }

  clearLogCache() {
    const { client } = this.props;
    client.writeQuery({
      query: INITIAL_ACTION_LOG_QUERY,
      variables: {
        skip: 0,
      },
      data: {
        actionLog: [],
      },
    });
  }

  render() {
    const {
      uuid, title, image,
    } = this.props;
    return (
      <Card>
        <CardImg top src={image} />
        <CardBody>
          <CardTitle>
            Що ж,
            {' '}
            {title.toLowerCase()}
            {' '}
             виявилося не так просто як ви собі думали.
          </CardTitle>
          <CardText>
            Тепер можна говорити відверто: ви неправильно оцінили ризики,
              супротивників та власне спорядження. Підготуйтеся як слід та
              доведь розпочате до логічного кінця.
          </CardText>
          <Mutation
            mutation={START_QUEST_MUTATION}
            variables={{ uuid, restart: true }}
            refetchQueries={[{ query: IS_ADVENTURE_RUNNING_QUERY },
              { query: LATEST_QUEST_QUERY }, { query: INITIAL_ACTION_LOG_QUERY },
              { query: GET_CURRENT_BACKGROUND_QUERY }]}
          >
            {restartQuest => (
              <Button color="danger" className="mt-2 btn btn-block" size="lg" onClick={event => this.handleFail(event, restartQuest)}>
                Повернутися та
                {' '}
                {title.toLowerCase()}
                !
              </Button>
            )}
          </Mutation>
        </CardBody>
      </Card>
    );
  }
}


LatestFailedQuest.propTypes = {
  uuid: PropTypes.string,
  title: PropTypes.string,
  image: PropTypes.string,
  client: PropTypes.objectOf(PropTypes.any),
};


export default withApollo(LatestFailedQuest);
