import React from 'react';
import {
  Button, Modal, ModalBody, ModalFooter,
  Card, CardBody, CardTitle, CardSubtitle, CardImg, CardText,
} from 'reactstrap';
import { Mutation } from 'react-apollo';
import PropTypes from 'prop-types';
import { getMapImage } from '../../../../helpers/utils';
import {
  AVALIABLE_QUESTS_QUERY, PENDING_QUESTS_QUERY,
  FIND_QUEST_MUTATION,
} from '../../../../queries';


class SearchNewQuests extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal,
    }));
  }

  handleSearch(event, searchQuest) {
    event.preventDefault();
    searchQuest().then(async () => {
      this.toggle();
    });
  }

  render() {
    const { quests } = this.props;
    const { modal } = this.state;
    return (
      <div style={{ textAlign: 'center' }}>
        <Button color="primary" onClick={this.toggle} className="mt-2 btn btn-block" size="lg">Шукати нові завдання</Button>
        <Modal centered isOpen={modal} toggle={this.toggle}>
          <ModalBody>
            { quests.length ? quests.map(quest => (
              <Card key={quest.uuid}>
                <CardImg top width="100%" src={getMapImage(quest.imageStr)} alt="" />
                <CardBody>
                  <CardTitle>{quest.title}</CardTitle>
                  <CardSubtitle>
                    Нагорода:
                    {' '}
                    {quest.rewardValue}
                    {' '}
                    <i className="mdi mdi-coins text-warning" />
                    ,
                    {' '}
                    {quest.experienceValue}
                    {' '}
                    пунктів досвіду
                  </CardSubtitle>
                  <CardText>{quest.description}</CardText>
                  <Mutation
                    mutation={FIND_QUEST_MUTATION}
                    variables={{ uuid: quest.uuid }}
                    refetchQueries={[{ query: AVALIABLE_QUESTS_QUERY },
                      { query: PENDING_QUESTS_QUERY }]}
                  >
                    {searchQuest => (
                      <Button color="primary" onClick={event => this.handleSearch(event, searchQuest)}>Додати до списку завдань</Button>
                    )}
                  </Mutation>
                </CardBody>
              </Card>
            )) : (
              <p>На жаль, наразі у вас замало інформації аби знайти будь-яке нове завдання. </p>
            )}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>Закрити</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

SearchNewQuests.propTypes = {
  quests: PropTypes.arrayOf(PropTypes.any),
};

export default SearchNewQuests;
