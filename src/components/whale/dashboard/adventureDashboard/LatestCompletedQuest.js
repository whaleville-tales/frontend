import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Button,
} from 'reactstrap';
import { Mutation, withApollo } from 'react-apollo';
import PropTypes from 'prop-types';
import {
  CLOSE_QUEST_MUTATION, LATEST_EVENT_QUERY,
  AVALIABLE_QUESTS_QUERY, PENDING_QUESTS_QUERY,
  IS_ADVENTURE_RUNNING_QUERY, GUILD_QUERY,
  RESTORE_PARTY_CHARACTERS_QUERY, EVENTS_QUERY,
  IS_GAME_FINISHED_QUERY,
  GET_CURRENT_BACKGROUND_QUERY,
  MARKET_INVENTORIES_QUERY,
  PARTY_INVENTORIES_QUERY,
  INITIAL_ACTION_LOG_QUERY,
  QUEST_CHAINS_QUERY,
} from '../../../../queries';


class LatestCompletedQuest extends React.Component {
  constructor(props) {
    super(props);
    this.handleClose = this.handleClose.bind(this);
    this.clearLogCache = this.clearLogCache.bind(this);
  }

  handleClose(event, closeQuest) {
    event.preventDefault();
    closeQuest().then(async () => {
      this.clearLogCache();
    });
  }

  clearLogCache() {
    const { client } = this.props;
    client.writeQuery({
      query: INITIAL_ACTION_LOG_QUERY,
      variables: {
        skip: 0,
      },
      data: {
        actionLog: [],
      },
    });
  }


  render() {
    const {
      uuid, title, report,
    } = this.props;
    return (
      <Card>
        <CardBody style={{ marginBottom: '-10px' }}>
          <CardTitle>
            Завдання виконане! Вам вдалося
            {' '}
            {title.toLowerCase()}
            !
          </CardTitle>
        </CardBody>
        <CardBody>
          <CardText>{report}</CardText>
          <Mutation
            mutation={CLOSE_QUEST_MUTATION}
            variables={{ uuid }}
            refetchQueries={[{ query: LATEST_EVENT_QUERY }, { query: AVALIABLE_QUESTS_QUERY },
              { query: PENDING_QUESTS_QUERY }, { query: IS_ADVENTURE_RUNNING_QUERY },
              { query: RESTORE_PARTY_CHARACTERS_QUERY }, { query: GUILD_QUERY },
              { query: EVENTS_QUERY }, { query: IS_GAME_FINISHED_QUERY },
              { query: GET_CURRENT_BACKGROUND_QUERY }, { query: MARKET_INVENTORIES_QUERY },
              { query: PARTY_INVENTORIES_QUERY }, { query: QUEST_CHAINS_QUERY }]}
          >
            {closeQuest => (
              <Button color="success" className="mt-2 btn btn-block" size="lg" onClick={event => this.handleClose(event, closeQuest)}>
                Завершити завдання
              </Button>
            )}
          </Mutation>
        </CardBody>
      </Card>
    );
  }
}


LatestCompletedQuest.propTypes = {
  uuid: PropTypes.string,
  title: PropTypes.string,
  report: PropTypes.string,
  client: PropTypes.objectOf(PropTypes.any),
};


export default withApollo(LatestCompletedQuest);
