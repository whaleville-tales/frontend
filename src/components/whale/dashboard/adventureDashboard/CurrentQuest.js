import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  CardImg,
} from 'reactstrap';
import PropTypes from 'prop-types';


function CurrentQuest(props) {
  const {
    title, image, body,
  } = props;
  return (
    <Card>
      <CardImg top width="100%" src={image} />
      <CardBody>
        <CardTitle>
Поточне завдання:
          {' '}
          {title.toLowerCase()}
        </CardTitle>
      </CardBody>

      <CardBody>
        <CardText>{body}</CardText>
      </CardBody>
    </Card>
  );
}


CurrentQuest.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
  image: PropTypes.string,
};


export default CurrentQuest;
