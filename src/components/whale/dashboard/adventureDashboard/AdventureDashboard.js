import React from 'react';
import {
  Row,
  Col,
} from 'reactstrap';
import { Query } from 'react-apollo';
import CurrentQuest from './CurrentQuest';
import Stats from '../Stats';
import ActionLog from './ActionLog';
import LatestCompletedQuest from './LatestCompletedQuest';
import {
  LATEST_QUEST_QUERY, PARTY_STATS_QUERY,
  INITIAL_ACTION_LOG_QUERY,
  LATEST_COMPLETED_QUEST_QUERY,
} from '../../../../queries';
import { getMapImage } from '../../../../helpers/utils';
import config from '../../../../helpers/config';


function filterDuplicates(oldLog, newLog) {
  const oldLogIds = oldLog.map(log => log.id);
  return newLog.filter((log) => {
    if (!oldLogIds.includes(log.id)) {
      return log;
    }
    return null;
  });
}


function AdventureDashboard() {
  return (
    <Row className="mx-auto">
      <Col sm="6">
        <Query query={PARTY_STATS_QUERY} pollInterval={config.POLL_INTERVAL}>
          {({ data, loading }) => {
            if (loading || !data) {
              return <div>Завантажується ...</div>;
            }
            return (
              <Row>
                {data.characters && (
                <Stats
                  characters={data.characters}
                />
                )}
              </Row>
            );
          }}
        </Query>
        <Query query={LATEST_QUEST_QUERY} pollInterval={config.POLL_INTERVAL}>
          {({ data }) => (
            <Row>
              {data && data.currentQuest && (
                <CurrentQuest
                  body={data.currentQuest.description}
                  title={data.currentQuest.title}
                  image={getMapImage(data.currentQuest.imageStr)}
                />
              )}
            </Row>
          )}
        </Query>
        <Query query={LATEST_COMPLETED_QUEST_QUERY} pollInterval={config.POLL_INTERVAL}>
          {({ data }) => (
            <Row>
              {data && data.latestCompletedQuest && (
                <LatestCompletedQuest
                  uuid={data.latestCompletedQuest.uuid}
                  title={data.latestCompletedQuest.title}
                  report={data.latestCompletedQuest.report}
                />
              )}
            </Row>
          )}
        </Query>
      </Col>
      <Col sm="6">
        <Query
          query={INITIAL_ACTION_LOG_QUERY}
          variables={{
            skip: 0,
          }}
        >
          {({ data, fetchMore }) => {
            if (!data) {
              return <div>Завантажується ...</div>;
            }
            return (
              data && (
                <div>
                  <ActionLog
                    logs={data.actionLog}
                    onLoadMore={index => fetchMore({
                      variables: {
                        skip: index,
                      },
                      updateQuery: (prev, { fetchMoreResult }) => {
                        if (!fetchMoreResult) return prev;
                        const actionLog = [...prev.actionLog,
                          ...filterDuplicates(prev.actionLog, fetchMoreResult.actionLog)];
                        return Object.assign({}, prev, {
                          actionLog,
                        });
                      },
                    })
                    }
                  />
                </div>
              )
            );
          }}
        </Query>
      </Col>
    </Row>
  );
}

export default AdventureDashboard;
