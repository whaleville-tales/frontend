/* eslint-disable react/no-unused-state */
import React from 'react';
import {
  Alert,
  Card,
  CardBody,
  CardSubtitle,
  CardText,
  Col,
  Row,
} from 'reactstrap';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import LoadingSpinner from '../../../../layout/loadingSpinner/LoadingSpinner';
import config from '../../../../helpers/config';


class ActionLog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: Date.now(),
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => this.setState({ time: Date.now() }), config.POLL_INTERVAL);
  }

  componentDidUpdate() {
    const { onLoadMore } = this.props;
    // FIXME
    // sometimes action log doesn't show latest events
    // it's because of backend not expected behavior on few
    // last action log records.
    // It starts to skip latest instead of skipping earlest records
    onLoadMore(0);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { logs } = this.props;
    return (
      <Card>
        <CardBody>
          <Row className="pb-3">
            <Col sm="6"><CardSubtitle className="my-auto">Зведення з полів</CardSubtitle></Col>
            <Col sm="6" className="text-right"><LoadingSpinner /></Col>
          </Row>
          <div
            className="comment-widgets scrollable"
            style={{ height: '600px' }}
          >
            <PerfectScrollbar>
              {!logs.length ? (
                <CardText>Завдання розпочалося. Приготуйтеся до пригоди.</CardText>
              ) : logs.sort((a, b) => (a.id > b.id ? -1 : 1)).map((log) => {
                if (log.logType === 'LOOT') {
                  return <Alert key={log.id} color="warning">{log.text}</Alert>;
                }
                if (log.logType === 'DEATH') {
                  return <Alert key={log.id} color="dark">{log.text}</Alert>;
                }
                if (log.logType === 'DAMAGE') {
                  return <Alert key={log.id} color="danger">{log.text}</Alert>;
                }
                if (log.logType === 'CHECK') {
                  return <Alert key={log.id} color="success">{log.text}</Alert>;
                }
                if (log.logType === 'SKILL' || log.logType === 'ITEM') {
                  return <Alert key={log.id} color="primary">{log.text}</Alert>;
                }
                return <Alert color="light" key={log.id}>{log.text}</Alert>;
              })}
            </PerfectScrollbar>
          </div>
        </CardBody>
      </Card>
    );
  }
}

ActionLog.propTypes = {
  logs: PropTypes.arrayOf(PropTypes.any),
  onLoadMore: PropTypes.func,
};


export default ActionLog;
