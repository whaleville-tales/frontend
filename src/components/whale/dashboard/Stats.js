import React from 'react';
import {
  Card,
  CardBody,
  CardSubtitle,
  Table,
} from 'reactstrap';
import PropTypes from 'prop-types';
import StatsItem from './StatsItem';
import { getCharacterImage, getClassName } from '../../../helpers/utils';

function Stats(props) {
  const { characters } = props;
  return (
    <Card className="col-sm-12">
      <CardBody>
        <CardSubtitle style={{ marginBottom: '-10px' }}>Гурт</CardSubtitle>
        <div>
          <Table className="stylish-table mb-0" responsive>
            <thead>
              <tr>
                <th className="text-muted font-medium border-top-0" />
                <th className="text-muted font-medium border-top-0">Ім&apos;я</th>
                <th className="text-muted font-medium border-top-0">Самопочуття</th>
                <th className="text-muted font-medium border-top-0">Виснаженість</th>
                <th className="text-muted font-medium border-top-0">
                    Досвід
                </th>
              </tr>
            </thead>
            <tbody>
              {characters && characters.map(character => (
                <StatsItem
                  key={character.uuid}
                  image={getCharacterImage(
                    character.sexType.toLowerCase(),
                    character.classType.toLowerCase(),
                    character.imageStr,
                  )}
                  username={character.name}
                  smtext={getClassName(character.classType)}
                  hitsCurrent={character.attributes.currentHp}
                  hitsMax={character.attributes.totalHp}
                  exhaustionCurrent={character.attributes.currentExhaustion}
                  exhaustionMax={character.attributes.totalExhaustion}
                  expCurrent={character.attributes.experience}
                  expMax={character.attributes.level.nextLevelMaxExperience}
                />
              ))}
            </tbody>
          </Table>
        </div>
      </CardBody>
    </Card>
  );
}

Stats.propTypes = {
  characters: PropTypes.arrayOf(PropTypes.any),
};


export default Stats;
