import React from 'react';
import { Query } from 'react-apollo';
import { IS_ADVENTURE_RUNNING_QUERY } from '../../../queries';
import RestDashboard from './restDashboard/RestDashboard';
import AdventureDashboard from './adventureDashboard/AdventureDashboard';

function Dashboard() {
  return (
    <Query query={IS_ADVENTURE_RUNNING_QUERY} pollInterval={4000}>
      {({ data, loading }) => {
        if (loading || !data) {
          return <div>Завантажується ...</div>;
        }
        if (data.isAdventureRunning) {
          return (
            <AdventureDashboard />
          );
        }
        return (
          <RestDashboard />
        );
      }}
    </Query>
  );
}

export default Dashboard;
