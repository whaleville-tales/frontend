import React from 'react';
import PropTypes from 'prop-types';
import Tippy from '@tippy.js/react';
import { getItemImage, getItemDescription } from '../../../helpers/utils';


function Icon(props) {
  const { item, isMarket } = props;
  const description = getItemDescription(item);
  let isVisiblePopover = false;
  let { price, title } = item;
  let borderColor = 'none';
  let backgroundColor = 'none';
  if (description.length > 0) {
    isVisiblePopover = true;
  }

  if (isMarket) {
    price = item.marketPrice;
  }
  if (item.itemSpecial === 'QUIVER') {
    title = `${title} (${item.quantity} штук)`;
  }

  if (item.itemRarity === 'LEGENDARY') {
    borderColor = '#7460ee';
    backgroundColor = '#f6f5ff';
  }
  if (item.itemRarity === 'MASTERPIECE') {
    borderColor = '#1e88e5';
    backgroundColor = '#ecf6ff';
  }
  if (item.itemRarity === 'RARE') {
    borderColor = '#22abbd';
    backgroundColor = '#f5feff';
  }


  return (
    <Tippy
      followCursor
      animation="fade"
      animateFill={false}
      enabled={isVisiblePopover}
      content={<span>{description}</span>}
    >
      <div className="d-flex align-items-center content p-2 mt-2 item" style={{ borderColor, backgroundColor }}>
        <img src={getItemImage(item.imageStr)} alt="" />
        <div className="ml-2"><span>{title}</span></div>
        <span className="pr-1 ml-auto text-warning" style={{ fontWeight: 500 }}>{price}</span>
        <i className="mdi mdi-coins text-warning" />
      </div>
    </Tippy>
  );
}

Icon.propTypes = {
  isMarket: PropTypes.bool,
  item: PropTypes.objectOf(PropTypes.any),
};

export default Icon;
