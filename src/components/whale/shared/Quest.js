import React from 'react';
import {
  Row,
  Col,
} from 'reactstrap';
import PropTypes from 'prop-types';


function Quest(props) {
  const {
    title, status, image, description,
  } = props;
  return (
    <div className="d-flex">
      <div className="ml-3">
        <div>
          {title}
          {' '}
            (
          {status}
            )
          <Row className="mt-3">
            <Col md="3" xs="12">
              <img
                src={image}
                alt=""
                className="img-fluid"
              />
            </Col>
            <Col md="9" xs="12">
              <p>
                {description}
              </p>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}


Quest.propTypes = {
  title: PropTypes.string,
  status: PropTypes.string,
  image: PropTypes.string,
  description: PropTypes.string,
};


export default Quest;
