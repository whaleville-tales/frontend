import React from 'react';
import {
  Card,
} from 'reactstrap';
import { Query } from 'react-apollo';
import { QUEST_CHAINS_QUERY } from '../../../queries';
import QuestsBoard from './QuestsBoard';


function Quests() {
  return (
    <Card>
      <Query query={QUEST_CHAINS_QUERY}>
        {({ data, loading }) => {
          if (loading || !data) {
            return <div>Завантажується ...</div>;
          }
          return (
            <QuestsBoard questChains={data.questChains} />
          );
        }}
      </Query>
    </Card>
  );
}

export default Quests;
