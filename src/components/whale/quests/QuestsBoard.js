import React from 'react';
import classnames from 'classnames';
import {
  Row,
  Col,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  CardBody,
} from 'reactstrap';
import PropTypes from 'prop-types';
import Quest from '../shared/Quest';
import { getMapImage, getQuestType } from '../../../helpers/utils';


class QuestsBoard extends React.Component {
  constructor(props) {
    super(props);
    const { questChains } = this.props;
    const filteredChains = questChains.filter(chain => chain.revealedQuests.length > 0);

    this.state = {
      activeTab: '0',
      chains: filteredChains,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle(tab) {
    const { activeTab } = this.state;
    if (activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    const { activeTab, chains } = this.state;
    return (
      <Col sm="12">
        <Nav pills className="custom-pills border-bottom">
          {chains.map((chain, i) => (
            chain.revealedQuests.length > 0 && (
            <NavItem key={chain.uuid}>
              <NavLink
                className={classnames({ active: activeTab === `${i}` })}
                onClick={() => {
                  this.toggle(`${i}`);
                }}
              >
                {chain.title}
              </NavLink>
            </NavItem>
            )
          ))}
        </Nav>
        <CardBody>
          <TabContent activeTab={activeTab} className="mt-3">
            {chains.map((chain, i) => (
              chain.revealedQuests.length > 0 && (
              <TabPane key={chain.uuid} tabId={`${i}`}>
                <Row>
                  <Col sm="12">
                    <div className="profiletimeline">
                      {chain.revealedQuests.map(quest => (
                        <div key={quest.uuid}>
                          <Quest
                            title={quest.title}
                            status={getQuestType(quest.questType)}
                            image={getMapImage(quest.imageStr)}
                            description={quest.description}
                          />
                          <hr />
                        </div>
                      ))}
                    </div>
                  </Col>
                </Row>
              </TabPane>
              )
            ))}
          </TabContent>
        </CardBody>
      </Col>
    );
  }
}

QuestsBoard.propTypes = {
  questChains: PropTypes.arrayOf(PropTypes.any),
};

export default QuestsBoard;
