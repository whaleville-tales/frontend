import React from 'react';
import {
  Row,
  Col, Card, CardBody, CardText,
} from 'reactstrap';
import { Timeline } from 'vertical-timeline-component-for-react';
import { Query } from 'react-apollo';
import Event from './Event';
import { EVENTS_QUERY } from '../../../queries';


function Events() {
  return (
    <div>
      <Row className="mx-auto">
        <Col sm="12">
          <Card>
            <CardBody className="">
              <CardText>
              Тут можна відслідкувати всі події, повʼязані з гуртом.
              </CardText>
            </CardBody>
          </Card>
        </Col>
        <Timeline lineColor="#ddd" animate={false}>
          <Query query={EVENTS_QUERY}>
            {({ data, loading }) => {
              if (loading || !data) {
                return <div>Завантажується ...</div>;
              }
              return (
                <Col sm="12">
                  {data.eventsAll.map(event => (
                    <Event
                      event={event}
                      key={event.uuid}
                    />
                  ))}
                </Col>
              );
            }}
          </Query>
        </Timeline>

      </Row>
    </div>
  );
}

export default Events;
