import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  CardImg,
} from 'reactstrap';
import PropTypes from 'prop-types';
import { TimelineItem } from 'vertical-timeline-component-for-react';
import { getEventImage, getEventTitle } from '../../../helpers/utils';


function Event(props) {
  const { event } = props;

  const createdAt = new Date(event.createdAt);

  return (
    <TimelineItem
      key={event.uuid}
      dateText={createdAt.toLocaleDateString('uk-ua')}
      style={{ color: '#22abbd' }}
      dateInnerStyle={{ background: '#22abbd' }}
    >
      <Card style={{ width: `${event.eventType !== 'QUESTCOMPLETE' ? '50%' : '100%'}` }}>
        <CardImg top src={getEventImage(event)} style={{ filter: `${event.eventType === 'DEATH' ? 'grayscale(100%)' : ''}` }} />
        <CardBody>
          <CardTitle>{getEventTitle(event)}</CardTitle>
          <CardText>
            {event.description}
          </CardText>
        </CardBody>
      </Card>
    </TimelineItem>
  );
}


Event.propTypes = {
  event: PropTypes.objectOf(PropTypes.any),
};


export default Event;
