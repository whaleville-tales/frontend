import React from 'react';
import {
  Col, Card, CardText, Row,
} from 'reactstrap';

function Thanks() {
  return (
    <div>
      <Row>
        <Col sm="12">
          <Card body>
            <CardText>
                Оповідки Китового були створені з використанням:
            </CardText>
            <CardText>
                1. Теми для побудови панелі адміністратора
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="https://www.wrappixel.com/templates/materialpro-react-redux-admin/">MaterialPro React Redux Admin</a>
            </CardText>
            <CardText>
                2. Набору карт від
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="https://www.2minutetabletop.com/">2-Minute Tabletop</a>
            </CardText>
            <CardText>
                3. Набору іконок
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="https://stevencolling.itch.io/inked-adventure-items">Inked Adventure Items</a>
            </CardText>
            <CardText>
                4. Генератора портретів
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="http://dmheroes.com/">DMHeroes</a>
            </CardText>
            <CardText>
                5. Інструментів:
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="https://www.djangoproject.com/">Django</a>
              ,
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="http://www.celeryproject.org/">Celery</a>
              ,
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="https://graphql.org/">GraphQL</a>
              ,
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="https://www.apollographql.com/">Apollo GraphQL</a>
              {' '}
              та
              {' '}
              <a rel="noopener noreferrer" target="_blank" href="https://reactjs.org/">React</a>
            </CardText>
          </Card>
          <Card body>
            <CardText>Автор: Олександр Ємець (oleksandr.ye@protonmail.com)</CardText>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default Thanks;
