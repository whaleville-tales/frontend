import React from 'react';
import {
  Col, Card, CardText, Row,
} from 'reactstrap';
import { Query } from 'react-apollo';
import CemetaryProfile from '../../layout/profile/CemetaryProfile';
import { CEMETARY_QUERY } from '../../queries';

function Cemetary() {
  return (
    <div>
      <Row>
        <Col sm="12">
          <Card body>
            <CardText>
                На міському цвинтарі спочиває багато гідного люду.
                На жаль, з часом свій шлях тут закінчують і ваші знайомі.
            </CardText>
          </Card>
        </Col>
      </Row>
      <Query query={CEMETARY_QUERY}>
        {({ data, loading }) => {
          if (loading || !data) {
            return <div>Завантажується ...</div>;
          }
          return (
            <Row>
              {data.characters.map(character => <div key={character.uuid} className="col-6 col-sm-4"><CemetaryProfile character={character} /></div>)}
            </Row>
          );
        }}
      </Query>
    </div>
  );
}

export default Cemetary;
