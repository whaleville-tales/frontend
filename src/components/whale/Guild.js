import React from 'react';
import {
  Card,
  CardText,
  Row,
  Col,
} from 'reactstrap';
import { Query } from 'react-apollo';
import GuildProfile from '../../layout/profile/GuildProfile';
import { GUILD_QUERY } from '../../queries';

function Guild() {
  return (
    <div>
      <Row>
        <Col sm="12">
          <Card body>
            <CardText>
                За відповідну суму грошей тут завжди
                знайдуться ті, хто захоче розділити з вами кілька пригод.
            </CardText>
          </Card>
        </Col>
      </Row>
      <Query query={GUILD_QUERY}>
        {({ data, loading, refetch }) => {
          if (loading || !data) {
            return <div>Завантажується ...</div>;
          }
          return (
            <Row>
              {data.characters.map(character => <div key={character.uuid} className="col-6 col-sm-4"><GuildProfile character={character} refetch={refetch} /></div>)}
            </Row>
          );
        }}
      </Query>


    </div>
  );
}

export default Guild;
