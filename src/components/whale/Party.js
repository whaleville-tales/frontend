import React from 'react';
import { Query } from 'react-apollo';
import { PARTY_QUERY } from '../../queries';
import CharacterProfile from '../../layout/profile/CharacterProfile';


function Party() {
  return (
    <Query query={PARTY_QUERY} pollInterval={4000}>
      {({ data, loading }) => {
        if (loading || !data) {
          return <div>Завантажується ...</div>;
        }
        return (
          <div className="row">
            {data.characters.map(character => <div key={character.uuid} className="col-6 col-sm-4"><CharacterProfile character={character} /></div>)}
          </div>
        );
      }}
    </Query>
  );
}

export default Party;
