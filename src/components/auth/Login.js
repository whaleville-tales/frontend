/* eslint-disable class-methods-use-this */
import React from 'react';
import {
  InputGroup,
  Input,
  Form,
  Row,
  Col,
  Button,

} from 'reactstrap';
import Cookies from 'js-cookie';
import { Mutation } from 'react-apollo';
import validators from './validators';
import backgroundImage from '../../assets/images/TradePort.jpg';
import { LOGIN_MUTATION } from '../../queries';
import LoadingSpinner from '../../layout/loadingSpinner/LoadingSpinner';


const sidebarBackground = {
  backgroundImage: `url(${backgroundImage})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
};

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      serverError: '',
      buttonDisabled: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.validators = validators;
    this.onInputChange = this.onInputChange.bind(this);
    this.doLogin = this.doLogin.bind(this);
    this.showErrors = this.showErrors.bind(this);
    this.formValidators = this.formValidators.bind(this);
  }

  onInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
    this.formValidators([event.target.name], event.target.value);
  }

  formValidators(fieldName, value) {
    this.validators[fieldName].errors = [];
    this.validators[fieldName].state = value;
    this.validators[fieldName].valid = true;
    this.validators[fieldName].rules.forEach((rule) => {
      if (rule.test instanceof RegExp) {
        if (!rule.test.test(value)) {
          this.validators[fieldName].errors.push(rule.message);
          this.validators[fieldName].valid = false;
        }
      } else if (typeof rule.test === 'function') {
        if (!rule.test(value)) {
          this.validators[fieldName].errors.push(rule.message);
          this.validators[fieldName].valid = false;
        }
      }
    });
  }

  validForm() {
    let status = true;
    Object.keys(this.validators).forEach((field) => {
      if (field === 'email' || field === 'password') {
        if (!this.validators[field].valid) {
          status = false;
        }
      }
    });
    return status;
  }

  showErrors(fieldName) {
    const validator = this.validators[fieldName];
    const result = '';
    if (validator && !validator.valid) {
      const errors = validator.errors.map((info, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <span className="error col-sm-12 text-center mb-3" key={index}>
          *
          {' '}
          {info}
          <br />
        </span>
      ));
      return (
        <div className="error col-sm-12 text-center mb-3">
          {errors}
        </div>
      );
    }
    return result;
  }

  doLogin(event, signinUser) {
    event.preventDefault();
    const { email, password } = this.state;
    if (email && password && this.validForm()) {
      this.setState({
        buttonDisabled: true,
      });
      signinUser().then(async ({ data }) => {
        Cookies.set('token', data.tokenAuth.token);
        window.location.href = '/';
      }).catch((error) => {
        if (error.graphQLErrors) {
          this.setState({
            buttonDisabled: false,
            serverError: error.graphQLErrors.map(x => x.message),
          });
        }
      });
    }
  }

  handleClick() {
    const elem = document.getElementById('loginform');
    elem.style.transition = 'all 2s ease-in-out';
    elem.style.display = 'none';
    document.getElementById('recoverform').style.display = 'block';
  }

  render() {
    const {
      email, password, serverError, buttonDisabled,
    } = this.state;
    let displayErrors;
    if (serverError) {
      displayErrors = serverError.map((element, key) => {
        let error = element;
        if (error.includes('enter valid credentials')) {
          error = 'Будь ласка, надайте правильні дані ';
        }
        const row = (
          <div
        // eslint-disable-next-line react/no-array-index-key
            key={key}
            className="error col-sm-12 text-center mb-3"
          >
            {error}

          </div>
        );
        return row;
      });
    }

    return (
      <div className="auth-wrapper  align-items-center d-flex" style={sidebarBackground}>
        <div className="container">
          <Mutation mutation={LOGIN_MUTATION} variables={{ email, password }}>
            {signinUser => (
              <div>
                <Row className="no-gutters justify-content-center">
                  <Col md="6" lg="4" className="bg-dark text-white">
                    <div className="p-4">
                      <h3 className="font-medium mb-3" style={{ color: 'white' }}>Привіт, Мандрівниче!</h3>
                      <p>
        Вітаємо тебе в Китовому - славетному рибальському
        поселенні при холодному, проте щедрому Західному Морі.
        Не зважаючи на те, що це - земля сильних духом, завжди є щілинка для скверни,
        що, коли її не випалювати накорені, швидко може загубити не тільки край,
        але й людські душі.
                      </p>
                      <p>
        Ця осінь в Китовому почалася неспокійно, і, схоже,
        без допомоги ззовні світло Надії може полишити ці землі набагато швидше,
        ніж багато хто може судити зараз.
                      </p>
                      <p>Китове чекає на тебе! А з ним - і його історії.</p>
                    </div>
                  </Col>
                  <Col md="6" lg="4" className="bg-white">
                    <div className="p-4">
                      <h3 className="font-medium mb-3">Ввійти в Китове</h3>
                      <Form className="mt-3" id="loginform" action="/dashbaord">
                        <InputGroup className="mb-2" size="lg">
                          <Input type="email" id="email" name="email" value={email} onChange={this.onInputChange} placeholder="Поштова скринька" />
                        </InputGroup>
                        {this.showErrors('email')}
                        <InputGroup className="mb-3" size="lg">
                          <Input type="password" id="password" name="password" value={password} onChange={this.onInputChange} placeholder="Пароль" />
                        </InputGroup>
                        {this.showErrors('password')}
                        <Row className="mb-3">
                          {displayErrors || ''}
                          <Col xs="12">
                            {buttonDisabled ? (
                              <div className="text-center"><LoadingSpinner /></div>
                            ) : (
                              <Button color="success" onClick={event => this.doLogin(event, signinUser)} className={`${this.validForm() ? '' : 'disabled'}`} size="lg" type="submit" block>Ввійти</Button>
                            )}
                          </Col>
                        </Row>
                        <div className="text-center">
                          Не зареєстровані?
                          {' '}
                          <a href="/authentication/register" className="text-success ml-1"><b>Реєструватися</b></a>
                        </div>
                      </Form>
                    </div>

                  </Col>
                </Row>
              </div>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}


export default Login;
