const validator = {

  email: {
    rules: [
      {
        test: /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i,
        message: 'Будь-ласка, введіть корректний E-mail',
      },
    ],
    errors: [],
    valid: false,
    state: '',
  },
  password: {
    rules: [
      {
        test: value => value.length >= 6,
        message: 'Пароль повинен мати більше 6 символів',
      },
    ],
    errors: [],
    valid: false,
    state: '',
  },
  username: {
    rules: [
      {
        test: /^[a-zA-Z_]+$/i,
        message: 'Цифри заборонені',
      },
    ],
    errors: [],
    valid: false,
    state: '',
  },
};

export default validator;
