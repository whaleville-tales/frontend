import React from 'react';
import {
  Input,
  CustomInput,
  FormGroup,
  Form,
  Row,
  Col,
  Button,
} from 'reactstrap';
import { Mutation, withApollo } from 'react-apollo';
import Cookies from 'js-cookie';
import PropTypes from 'prop-types';
import validators from './validators';
import backgroundImage from '../../assets/images/TradePort.jpg';
import LoadingSpinner from '../../layout/loadingSpinner/LoadingSpinner';
import { REGISTER_MUTATION, LOGIN_MUTATION } from '../../queries';


const sidebarBackground = {
  backgroundImage: `url(${backgroundImage})`,
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
};

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      serverError: '',
      buttonDisabled: false,
    };
    this.validators = validators;
    this.onInputChange = this.onInputChange.bind(this);
    this.doRegister = this.doRegister.bind(this);
    this.showErrors = this.showErrors.bind(this);
    this.formValidators = this.formValidators.bind(this);
  }

  onInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
    this.formValidators([event.target.name], event.target.value);
  }

  formValidators(fieldName, value) {
    this.validators[fieldName].errors = [];
    this.validators[fieldName].state = value;
    this.validators[fieldName].valid = true;
    this.validators[fieldName].rules.forEach((rule) => {
      if (rule.test instanceof RegExp) {
        if (!rule.test.test(value)) {
          this.validators[fieldName].errors.push(rule.message);
          this.validators[fieldName].valid = false;
        }
      } else if (typeof rule.test === 'function') {
        if (!rule.test(value)) {
          this.validators[fieldName].errors.push(rule.message);
          this.validators[fieldName].valid = false;
        }
      }
    });
  }

  validForm() {
    let status = true;
    Object.keys(this.validators).forEach((field) => {
      if (field === 'email' || field === 'password') {
        if (!this.validators[field].valid) {
          status = false;
        }
      }
    });
    return status;
  }

  showErrors(fieldName) {
    const validator = this.validators[fieldName];
    const result = '';
    if (validator && !validator.valid) {
      const errors = validator.errors.map((info, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <span className="error" key={index}>
            *
          {' '}
          {info}
          <br />
        </span>
      ));

      return <div className="error mb-2">{errors}</div>;
    }
    return result;
  }

  doRegister(event, registerUser) {
    const { email, password } = this.state;
    event.preventDefault();
    if (email && password && this.validForm()) {
      this.setState({
        buttonDisabled: true,
      });
      registerUser().then(async ({ data }) => {
        const { client } = this.props;
        if (data.register.response) {
          client.mutate({
            mutation: LOGIN_MUTATION,
            variables: { email, password },
          // eslint-disable-next-line no-shadow
          }).then(async ({ data }) => {
            await Cookies.set('token', data.tokenAuth.token);
          }).then(() => {
            window.location.href = '/';
          });
        }
      }).catch((error) => {
        if (error.graphQLErrors) {
          this.setState({
            buttonDisabled: false,
            serverError: error.graphQLErrors.map(x => x.message),
          });
        }
      });
    }
  }

  render() {
    const {
      email, password, serverError, buttonDisabled,
    } = this.state;
    let displayErrors;
    if (serverError) {
      displayErrors = serverError.map((element, key) => {
        let error = element;
        if (error.includes('already exists')) {
          error = 'Ця електронна скринька вже зайнята';
        }
        const row = (
          <div
        // eslint-disable-next-line react/no-array-index-key
            key={key}
            className="error col-sm-12 text-center mb-3"
          >
            {error}

          </div>
        );
        return row;
      });
    }
    return (
      <div
        className="auth-wrapper  align-items-center d-flex"
        style={sidebarBackground}
      >
        {/*--------------------------------------------------------------------------------*/}
        {/* Login Cards */}
        {/*--------------------------------------------------------------------------------*/}
        <div className="container">
          <Mutation mutation={REGISTER_MUTATION} variables={{ email, password }}>
            {registerUser => (
              <div>
                <Row className="no-gutters justify-content-center">
                  <Col md="6" lg="4" className="bg-dark text-white">
                    <div className="p-4">
                      <h3 className="font-medium mb-3" style={{ color: 'white' }}>Привіт, Мандрівниче!</h3>
                      <p>
                    Вітаємо тебе в Китовому - славетному рибальському
                    поселенні при холодному, проте щедрому Західному Морі.
                    Не зважаючи на те, що це - земля сильних духом, завжди є щілинка для скверни,
                    що, коли її не випалювати накорені, швидко може загубити не тільки край,
                    але й людські душі.
                      </p>
                      <p>
                    Ця осінь в Китовому почалася неспокійно, і, схоже,
                    без допомоги ззовні світло Надії може полишити ці землі набагато швидше,
                    ніж багато хто може судити зараз.
                      </p>
                      <p>Китове чекає на тебе! А з ним - і його історії.</p>
                    </div>
                  </Col>
                  <Col md="6" lg="4" className="bg-white">
                    <div className="p-4">
                      <h3 className="font-medium mb-3">Реєстрація в Китовому</h3>
                      <Form className="mt-3" id="loginform" action="/dashbaord" onSubmit={event => this.doRegister(event, registerUser)}>
                        <FormGroup className="mb-3">
                          <Input
                            type="email"
                            value={email}
                            onChange={this.onInputChange}
                            name="email"
                            id="email"
                            placeholder="Електронна скринька"
                            bsSize="lg"
                          />
                        </FormGroup>
                        {this.showErrors('email')}
                        <FormGroup className="mb-3">
                          <Input
                            type="password"
                            value={password}
                            onChange={this.onInputChange}
                            name="password"
                            id="password"
                            placeholder="Пароль"
                            bsSize="lg"
                          />
                        </FormGroup>
                        {this.showErrors('password')}
                        <CustomInput
                          type="checkbox"
                          id="сustomCheckbox"
                          label="Шаную права і свободи жителів Китового"
                        />
                        <Row className="mb-3 mt-3">
                          {displayErrors || ''}
                          <Col xs="12">
                            {buttonDisabled ? (
                              <div className="text-center"><LoadingSpinner /></div>
                            ) : (
                              <Button
                                className={`text-uppercase ${
                                  this.validForm() ? '' : 'disabled'
                                }`}
                                color="success"
                                size="lg"
                                type="submit"
                                block
                              >
                          Зареєструватися
                              </Button>
                            )}
                          </Col>
                        </Row>
                        <div className="text-center">
                      Зареєстровані?
                          {' '}
                          <a
                            href="/authentication/login"
                            className="text-success ml-1"
                          >
                            <b>Ввійдіть тут</b>
                          </a>
                        </div>
                      </Form>
                    </div>
                  </Col>
                </Row>
              </div>
            )}
          </Mutation>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  client: PropTypes.objectOf(PropTypes.any),
};


export default withApollo(Register);
