import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import indexRoutes from './routes';
import { configureStore } from './redux/store';


const App = () => (
  <Provider store={configureStore()}>
    <Router basename="/">
      <Switch>
        {
        indexRoutes.map(
          // eslint-disable-next-line react/no-array-index-key
          (prop, key) => <Route path={prop.path} key={key} component={prop.component} />,
        )
        }
      </Switch>
    </Router>
  </Provider>
);

export default App;
