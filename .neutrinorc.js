const path = require('path');
const airbnb = require('@neutrinojs/airbnb');
const react = require('@neutrinojs/react');
const jest = require('@neutrinojs/jest');
const styleLoader = require('@neutrinojs/style-loader');

module.exports = {
  options: {
    root: __dirname,
    output: './DIST',
  },
  use: [
      airbnb(
      {
        eslint: {
          rules: {
            'no-underscore-dangle': 0,
            'react/jsx-filename-extension': 0,
            'react/require-default-props': 0,
            'import/prefer-default-export': 0,
            'prefer-promise-reject-errors': 0,
            'jsx-a11y/click-events-have-key-events': 0,
            'jsx-a11y/no-noninteractive-element-interactions': 0,
            'jsx-a11y/media-has-caption': 0,
          },
        },
      }),
      react(
      {
        publicPath: '/',
        devServer: {
          contentBase: path.join(__dirname, 'build'),
          watchContentBase: true,
          publicPath: '/',
          host: '0.0.0.0',
          compress: true,
          headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
            'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
          },
        },
        style: {
          test: /\.(css|styl|scss)$/,
          modulesTest: /\.module\.(css|styl|scss)$/,
          loaders: [
            {
              loader: require.resolve('sass-loader'),
              useId: 'sass',
            },
          ],
        },
        html: {
          inject: false,
          template: 'src/index.tpl',
          filename: './index.html',
          minify: {
            collapseWhitespace: false,
          },
        },
      }),
    jest(),
  ],
};
