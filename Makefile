
build:
	yarn install && yarn build

run:
	yarn start

serve-dist:
	cd DIST && http-server

test:
	yarn test

lint:
	yarn lint

install:
	yarn install && yarn start

clean:
	rm -rf DIST node_modules
